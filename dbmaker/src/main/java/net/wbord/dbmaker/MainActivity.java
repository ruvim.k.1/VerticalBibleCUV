package net.wbord.dbmaker;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.wbord.verticalbible.Bible;
import net.wbord.verticalbible.BibleDbAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity { 
	private static final String BIBLE_TEXT_TRADITIONAL = "bible-cuv-zhhant.bc.txt"; 
	private static final String BIBLE_TEXT_SIMPLIFIED = "bible-cuv-zhhans.bc.txt"; 
	private static final String STRONG_TRADITIONAL = "zhhant.sn.txt"; 
	private static final String STRONG_SIMPLIFIED = "zhhans.sn.txt"; 
	
	boolean useSimplified = false; 
	
	TextView vStatus = null; 
	Button vBuildButton = null; 
	
	BibleDbAdapter adapter = new BibleDbAdapter (this); 
	
	Thread work_build = new Thread (new Runnable () {
		@Override
		public void run () { 
			int mode = 0; 
			int books_loaded = 0; 
			int headers_loaded = 0; 
			int verses_loaded = 0; 
			final int MODE_BIBLE = 1; 
			final int MODE_CHAPTER = 2; 
			final int MODE_CONTEXT = 3; 
			//setStatusText ("Working ... "); 
			working = true; 
			File dir = Environment.getExternalStoragePublicDirectory (Environment.DIRECTORY_DOWNLOADS); 
			if (dir.canWrite ()) { 
				adapter.localOpen ("bible"); 
				adapter.clear (true); // Clear and reset to work with FTS3 extension. 
				Bible.ReadNode node; 
				InputStream mainText = null; 
				BufferedReader reader = null; 
				try { 
//					mainText = getAssets ().open (getBibleTextFilename ()); 
					mainText = getFromHTTP ("http://192.168.0.5/BibleText/" + getBibleTextFilename ()); 
					reader = new BufferedReader (new InputStreamReader (mainText, "UTF-8")); 
					String s; 
					while ((s = reader.readLine ()) != null) { 
						if (s.isEmpty ()) continue; 
						if (s.charAt (0) == '*') { 
							// Section: 
							String section = s.substring (1); 
							switch (section) { 
								case "Bible": 
									mode = MODE_BIBLE; 
									break; 
								case "Chapter": 
									mode = MODE_CHAPTER; 
									break; 
								case "Context": 
									mode = MODE_CONTEXT; 
									break; 
								default: 
							} 
						} else if (s.charAt (0) == '#') continue; 
						else { 
							if (mode == MODE_CHAPTER) { 
								String columns [] = s.split (Bible.ReadNode.DELIM_COLUMN);
								if (columns.length < 4) continue; 
								Bible.Book book = new Bible.Book (columns[0], columns[1], columns[2], 
									(books_loaded < Bible.OT_BOOK_COUNT)? Bible.IS_OT: Bible.IS_NT); 
								book.setChapterCount (Integer.parseInt (columns[3])); 
								if (adapter.books.insert (book) > 0) 
									books_loaded++; 
							} else if (mode == MODE_CONTEXT) 
								if (adapter.verses.insert (node = Bible.ReadNode.parseText (s)) > 0) { 
									if (node.getType ().equals (Bible.ReadNode.TYPE_VERSE)) verses_loaded++; 
									else if (node.getType ().equals (Bible.ReadNode.TYPE_HEADER)) headers_loaded++; 
								} 
						} 
						if (Math.random () > 1f / 64) continue; 
						// Randomly Update View: 
						setStatusText ("Working ... \n\nLoaded: \n\n" + 
												 "Books: " + books_loaded + "\n" + 
												 "Headers: " + headers_loaded + "\n" + 
												 "Verses: " + verses_loaded + "\n" + 
						""); 
					} 
				} catch (IOException err) { 
					Log.e ("DbMaker", "IO Exception"); 
				} finally { 
					if (reader != null) try { reader.close (); } catch (IOException err) {} 
					if (mainText != null) try { mainText.close (); } catch (IOException err) {} 
				} 
				adapter.copyToDir (dir); 
				adapter.close (); 
			} else 
				throw new IllegalStateException ("Cannot write to directory: " + dir.getAbsolutePath ()); 
			working = false; 
			setStatusText ("Done! \n\nLoaded: \n\n" + 
									 "Books: " + books_loaded + "\n" + 
									 "Headers: " + headers_loaded + "\n" + 
									 "Verses: " + verses_loaded + "\n" + 
			""); 
		}
	}); 
	boolean working = false; 
	
	private void setStatusText (final String text) { 
		runOnUiThread (new Runnable () {
			@Override
			public void run () {
				vStatus.setText (text); 
			}
		});
	} 
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main); 
		vStatus = (TextView) findViewById (R.id.status_text); 
		vBuildButton = (Button) findViewById (R.id.build_button); 
		vBuildButton.setOnClickListener (new View.OnClickListener () {
			@Override
			public void onClick (View v) {
				if (working) return; 
				// Build! 
				work_build.start (); 
			}
		}); 
		adapter.DB_NAME = "db_bible";  
	}
	
	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater ().inflate (R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId ();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected (item);
	} 
	
	public InputStream getFromHTTP (String the_url) { 
		InputStream stream = null; 
		try { 
			URL url = new URL (the_url);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection (); 
			connection.setReadTimeout (4000); 
			connection.setConnectTimeout (4000); 
			connection.setRequestMethod ("GET"); 
			connection.setDoInput (true); 
			connection.connect (); 
			int response = connection.getResponseCode (); 
			if (response == 200) stream = connection.getInputStream (); 
			else 
				Log.e ("DbMaker", "Status code is not 200 (HTTP, 200 = OK)"); 
		} catch (MalformedURLException err) { 
			Log.e ("DbMaker", "Malformed URL"); 
		} catch (IOException err) { 
			Log.e ("DbMaker", "IO exception"); 
		} 
		return stream; 
	} 
	
	public boolean isUsingSimplified () { return useSimplified; } 
	public String getBibleTextFilename () { 
		if (isUsingSimplified ()) return BIBLE_TEXT_SIMPLIFIED; 
		else return BIBLE_TEXT_TRADITIONAL; 
	} 
	public String getStrongFilename () { 
		if (isUsingSimplified ()) return STRONG_SIMPLIFIED; 
		else return STRONG_TRADITIONAL; 
	} 
}
