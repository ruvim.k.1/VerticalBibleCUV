package net.wbord.biblechineseunionversionvertical;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.wbord.verticalbible.Bible;
import net.wbord.verticalbible.BibleDbAdapter;
import net.wbord.verticalbible.ChapterSelectFragment;
import net.wbord.verticalbible.ChooseBookFragment;
import net.wbord.verticalbible.HistoryDbAdapter;
import net.wbord.verticalbible.ReadChapterFragment;
import net.wbord.verticalbible.ReadingHistory;
import net.wbord.verticalbible.VerticalTextView;


public class ChooseBook extends AppCompatActivity 
		implements ChooseBookFragment.BookSelectListener, 
						   ChapterSelectFragment.OnFragmentInteractionListener { 
	public static final String STATE_CHAPTER = "chapter"; 
	public static final String STATE_READSPACE = "readspace_item"; 
	public static final String STATE_LOADDESC = "load_descriptor"; 
	
	public static final String STATE_BOOK_SEL = "book_select"; 
	public static final String STATE_CHAPTER_SEL = "chapter_select"; 
	
	SharedPreferences pref = null; 
	MandarinDbPath dbPath = new MandarinDbPath (); 
	
	HistoryDbAdapter history = new HistoryDbAdapter (this); 
	BibleDbAdapter bible = new BibleDbAdapter (this); 
	
	Bible.Book bookList [] = null; 
	ChooseBookFragment chooseBook = null; 
	ChapterSelectFragment chapterSelect = null; 
	
	LinearLayout readingSpace = null; 
	
	ReadChapterFragment prevReadspaceItem = null; 
	Bible.LoadPageDescriptor loadDescriptor = new Bible.LoadPageDescriptor (); 
	
	public static final int [] section_ids__in_order = new int [] { 
																		  ChooseBookFragment.SECTION_ZUI_ZIN, 
																		  ChooseBookFragment.SECTION_XIN_YUE, 
																		  ChooseBookFragment.SECTION_JIU_YUE 
	}; 
	private int id_index_offset = 1; 
	
	private int readspaceItemCount = 0; 
	private int readspaceOpenCount = 0; 
	
	public float minReadspaceItemHeight = 2; // in inches 
	
	BibleDbAdapter.OnLoadFinished<Bible.Book> booksLoaded =
			new BibleDbAdapter.OnLoadFinished<Bible.Book> () {
				@Override
				public void run () {
					if (result.length < 1) {
						// No books! 
//					new AlertDialog.Builder (ChooseBook.this) 
//							.setMessage (R.string.msg_need_init_data) 
//							.setPositiveButton (R.string.btn_lbl_load_now, 
//													   new DialogInterface.OnClickListener () {
//								@Override
//								public void onClick (DialogInterface dialog, int which) {
//									dialog.dismiss (); 
//									Intent loadNow = new Intent (ChooseBook.this, InitDataActivity.class); 
//									startActivityForResult (loadNow, InitDataActivity.ACTION_INIT_DATA); 
//								}
//							}) 
//							.show (); 
						return;
					}
					bookList = result;
					chooseBook.setBookNames (result); 
					if (chapterSelect != null) 
						chapterSelect.setChapterCount (0); 

//					final ProgressDialog progressDialog =
//							ProgressDialog.show (ChooseBook.this, getText (R.string.msg_please_wait),
//														getText (R.string.msg_preparing_search_data),
//														true);
//					Thread thread = new Thread (new Runnable () {
//						@Override
//						public void run () {
//							// Check for FTS3: 
//							if (bible.areTablesFTS3 ()) {
//								progressDialog.dismiss ();
//								return;
//							}
//							// Convert to FTS3: 
//							bible.convertTablesToFTS3 ();
//							progressDialog.dismiss ();
//						}
//					});
//					thread.start ();
				}
			}; 
	
	@Override
	protected void onCreate (final Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_choose_book); 
		
		pref = PreferenceManager.getDefaultSharedPreferences (this); 
		
		boolean hasChapterSel = false; 
		boolean hasBookSel = false; 
		
//		bible.localOpen (dbPath.getDbName ()); 
		bible.open (dbPath.getDbName ()); 
		bible.simplifyChinese = 
				pref.getBoolean (SettingsActivity.KEY_SIMPLIFY_CHINESE, false); 
		
		// Set up the action bar.
		final ActionBar actionBar = getSupportActionBar (); 
		
		if (savedInstanceState != null && savedInstanceState.containsKey (STATE_CHAPTER_SEL)) { 
			chapterSelect = (ChapterSelectFragment) getSupportFragmentManager () 
					.getFragment (savedInstanceState, STATE_CHAPTER_SEL); 
			hasChapterSel = true; 
		} else 
			chapterSelect = ChapterSelectFragment.newInstance (0); 
		chapterSelect.setInteractionListener (this); 
		
		Bundle args = new Bundle (); 
		if (savedInstanceState != null && savedInstanceState.containsKey (STATE_BOOK_SEL)) { 
			chooseBook = (ChooseBookFragment) getSupportFragmentManager () 
					.getFragment (savedInstanceState, STATE_BOOK_SEL); 
			hasBookSel = true; 
		} else 
			chooseBook = new ChooseBookFragment (); 
		if (savedInstanceState != null) { 
			if (savedInstanceState.containsKey (ChooseBookFragment.KEY_SECTION_ID)) 
				args.putInt (ChooseBookFragment.KEY_SECTION_ID, 
									savedInstanceState.getInt (ChooseBookFragment.KEY_SECTION_ID)); 
			if (savedInstanceState.containsKey (Bible.STATE_BOOK)) {
				Bible.Book sel = new Bible.Book (); 
				sel.loadState (savedInstanceState.getBundle (Bible.STATE_BOOK)); 
				chapterSelect.setChapterCount (sel.getChapterCount ()); 
				chooseBook.setSelected (sel); 
			} 
			if (savedInstanceState.containsKey (STATE_CHAPTER)) 
				chapterSelect.setSelectedChapter (savedInstanceState.getInt (STATE_CHAPTER)); 
			if (savedInstanceState.containsKey (STATE_LOADDESC)) 
				loadDescriptor.loadState (savedInstanceState.getBundle (STATE_LOADDESC)); 
			if (savedInstanceState.containsKey (STATE_READSPACE)) { 
				ReadChapterFragment restoreFragment = (ReadChapterFragment) getSupportFragmentManager () 
						.getFragment (savedInstanceState, STATE_READSPACE); 
				setupReadspaceFragment (restoreFragment); 
			} 
		} 
		if (!hasBookSel) 
			chooseBook.setArguments (args); 
		chooseBook.setBookSelectListener (this); 
		
		bible.books.fetchAsync (booksLoaded); 
		
		final TabLayout tabs = (TabLayout) findViewById (R.id.yue_tabs); 
		history.open (); 
		history.historyTable.fetchAsync (new HistoryDbAdapter.OnLoadFinished<ReadingHistory.Entry> () { 
			@Override 
			public void run () { 
				TabLayout.Tab tabRecent, tabOld, tabNew; 
				tabRecent = null; 
				if (result.length > 0) { 
					tabs.addTab (tabRecent = tabs.newTab ()); 
					tabRecent.setText (R.string.recent); 
					id_index_offset = 0; 
				} else chooseBook.setCurrentSection (ChooseBookFragment.SECTION_XIN_YUE); 
				tabs.addTab (tabNew = tabs.newTab ()); 
					tabNew.setText (R.string.xin_yue); 
				tabs.addTab (tabOld = tabs.newTab ()); 
					tabOld.setText (R.string.jiu_yue);
				if (savedInstanceState != null) { 
					int section = savedInstanceState.getInt (ChooseBookFragment.KEY_SECTION_ID); 
					if (section == ChooseBookFragment.SECTION_ZUI_ZIN && tabRecent != null) tabRecent.select (); 
					else if (section == ChooseBookFragment.SECTION_JIU_YUE) tabOld.select (); 
					else if (section == ChooseBookFragment.SECTION_XIN_YUE) tabNew.select (); 
					chooseBook.setCurrentSection (section); 
				} 
				chooseBook.setRecent (result); 
			} 
		}); 
		tabs.setOnTabSelectedListener (new TabLayout.OnTabSelectedListener () {
			@Override
			public void onTabSelected (TabLayout.Tab tab) {
				chooseBook.setCurrentSection (section_ids__in_order [tab.getPosition () + id_index_offset]); 
			}

			@Override
			public void onTabUnselected (TabLayout.Tab tab) {
				
			}

			@Override
			public void onTabReselected (TabLayout.Tab tab) {
				
			}
		}); 
		
		FragmentTransaction ft = getSupportFragmentManager ().beginTransaction (); 
		if (!hasBookSel) 
			ft.replace (R.id.book_select_fragment_placeholder, chooseBook); 
		if (!hasChapterSel) 
			ft.replace (R.id.chapter_select_fragment_placeholder, chapterSelect); 
		ft.commit (); 
		
		readingSpace = (LinearLayout) findViewById (R.id.reading_space); 
	} 
	
	@Override 
	public void onSaveInstanceState (Bundle outState) { 
		super.onSaveInstanceState (outState); 
		outState.putInt (ChooseBookFragment.KEY_SECTION_ID, chooseBook.getCurrentSection ()); 
		if (chooseBook.getSelected () != null) 
			outState.putBundle (Bible.STATE_BOOK, chooseBook.getSelected ().saveState ()); 
		outState.putInt (STATE_CHAPTER, chapterSelect.getSelectedChapter ()); 
		outState.putBundle (STATE_LOADDESC, loadDescriptor.saveState ()); 
		getSupportFragmentManager () 
				.putFragment (outState, STATE_BOOK_SEL, chooseBook); 
		if (chapterSelect != null) 
			getSupportFragmentManager () 
				.putFragment (outState, STATE_CHAPTER_SEL, chapterSelect);
		if (prevReadspaceItem != null) 
			getSupportFragmentManager () 
					.putFragment (outState, STATE_READSPACE, prevReadspaceItem); 
//		super.onSaveInstanceState (outState); 
	} 


	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater ().inflate (R.menu.menu_choose_book, menu);
		return true;
	} 
	
	@Override 
	public void onPause () { 
		bible.close (); 
		history.close (); 
		super.onPause (); 
	} 
	@Override 
	public void onResume () { 
		super.onResume (); 
		history.open (); 
		bible.open (dbPath.getDbName ()); 
		boolean simp = pref.getBoolean (SettingsActivity.KEY_SIMPLIFY_CHINESE, false); 
		if (simp != bible.simplifyChinese) { 
			bible.simplifyChinese = simp; 
			bible.books.fetchAsync (booksLoaded); 
		} 
	} 
	
	@Override 
	public void onDestroy () { 
		history.close (); 
		bible.close (); 
		super.onDestroy (); 
	} 
	
	@Override public void onActivityResult (int requestCode, int resultCode, Intent data) { 
		switch (requestCode) { 
			case InitDataActivity.ACTION_INIT_DATA: 
				switch (resultCode) { 
					case InitDataActivity.RESULT_BAD: 
						Toast.makeText (this, R.string.msg_load_data_err, Toast.LENGTH_LONG) 
								.show (); 
						break; 
					case InitDataActivity.RESULT_OK: 
						Toast.makeText (this, R.string.msg_load_data_ok, Toast.LENGTH_SHORT) 
								.show (); 
						if (Build.VERSION.SDK_INT < 11) { 
							Intent intent = getIntent (); 
							finish (); 
							startActivity (intent); 
						} else recreate (); 
						break; 
					default: 
				} 
				break; 
			default: 
		} 
	} 
	
	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId ();
		
		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) { 
			Intent settings = new Intent (this, SettingsActivity.class); 
			startActivity (settings); 
			return true;
		} else if (id == R.id.action_search) { 
			onSearchRequested (); 
			return true; 
		} else if (id == R.id.action_random_place) { 
			goToRandom (); 
			return true; 
		} 
		
		return super.onOptionsItemSelected (item);
	} 
	
	private boolean randomTapped = false; 
	protected void goToRandom () { 
		if (randomTapped) return; // Already processing a random request. 
		randomTapped = true; 
		// Define a callback: 
		BibleDbAdapter.OnLoadFinished<Bible.ReadNode> launchRandom = 
				new BibleDbAdapter.OnLoadFinished<Bible.ReadNode> () {
					@Override
					public void run () { 
						if (result.length == 0) { 
							Toast.makeText (ChooseBook.this, R.string.msg_sorry_error, Toast.LENGTH_SHORT) 
									.show (); 
							randomTapped = false; // Done processing. 
							return; 
						} 
						Bible.ReadNode randomRead = result[0]; 
						Bible.LoadPageDescriptor randomLoad = new Bible.LoadPageDescriptor (); 
						randomLoad.getPlace ().set (randomRead.getPlace ()); 
						Bible.Book theBook = null; 
						// Find the book: 
						if (bookList != null) 
							for (Bible.Book book : bookList) { 
								if (!book.getSym ().equals (randomLoad.getPlace ().book)) 
									continue; 
								theBook = book; 
								break; 
							} 
						// Go to the random place: 
						Intent randomPlaceIntent = new Intent (ChooseBook.this, ReadChapter.class); 
						if (theBook != null) 
							randomPlaceIntent.putExtra (Bible.STATE_BOOK, 
															   theBook.saveState ()); 
						randomPlaceIntent.putExtra (Bible.STATE_LOAD, 
														   randomLoad.saveState ()); 
						startActivity (randomPlaceIntent); 
						randomTapped = false; // Done processing. 
					} 
				}; 
		// Fetch random verse: 
		Bible.Book sel = chooseBook.getSelected (); 
		if (sel != null) 
			bible.verses.fetchRandomAsync (sel, launchRandom); 
		else 
			bible.verses.fetchRandomAsync (launchRandom); 
	} 
	
	@Override public void onBookSelected (Bible.Book book) { 
		if (book.equals (chooseBook.getSelected ())) { 
			// If second time tapping on same book, clear selection, eh? 
			chooseBook.setSelected (null); 
			chapterSelect.setChapterCount (0); 
			chapterSelect.setSelectedChapter (-1); 
		} else { 
			chooseBook.setSelected (book); 
			chapterSelect.setChapterCount (book.getChapterCount ()); 
			chapterSelect.setSelectedChapter (-1); 
		} 
	} 
	@Override public void onChapterSelect (int chapterNumber) {
		final Bible.Book book = chooseBook.getSelected (); 
		if (book == null) return; 
		chapterSelect.setSelectedChapter (chapterNumber); 
		loadDescriptor.getPlace ().set (book.getSym (), chapterNumber, 1); 
		if (!isReadingSpaceLargeEnough () && readspaceOpenCount < 1) { 
			// Use the new-activity approach. Reading space is too small. 
			Intent intent = new Intent (this, ReadChapter.class); 
			intent.putExtra (Bible.STATE_BOOK, book.saveState ()); 
			intent.putExtra (Bible.STATE_LOAD, loadDescriptor.saveState ()); 
			startActivity (intent); 
			return; 
		} 
		// Use the reading-space-and-fragments approach. 
		setupReadspaceFragment (null); 
	} 
	private void setupReadspaceFragment (final ReadChapterFragment readChapter) { 
		final Bible.Book book = chooseBook.getSelected (); 
		if (book == null) { 
			if (readChapter != null) 
				getSupportFragmentManager () 
						.beginTransaction () 
						.remove (readChapter) 
						.commit (); 
			return; 
		} 
		final ControlPanelAdapter controlPanelAdapter = new ControlPanelAdapter ();
		final ReadChapterFragment readChapterFragment; 
		if (readChapter == null) 
			readChapterFragment = new ReadChapterFragment (); 
		else 
			readChapterFragment = readChapter; 
		readChapterFragment.setStateLoading (); // Set as loading, for now. 
			// It will change back to either 'ready' or 'empty' by itself. 
		readChapterFragment.setOnVerseInteractionListener (new ReadChapterFragment.OnVerseInteractionListener () {
			@Override
			public boolean onVerseClick (Bible.ReadNode node) {
				return false;
			} 
			@Override
			public boolean onVerseLinkClick (Bible.ReadNode node, int linkIndex) {
				return false;
			} 
			@Override
			public void onVerseLongClick (Bible.ReadNode node) {
				final String place = node.getType ().equals (Bible.ReadNode.TYPE_VERSE)? 
									   " " + 
											   "(" + 
											   book 
													   .getShortname () + 
											   node.getPlace ().getChapter () + ":" + 
											   node.getPlace ().getVerse () + ")": 
									   ""; 
				final EditText vText = (EditText) 
						   LayoutInflater.from (ChooseBook.this) 
								   .inflate (R.layout.verse_look_edittext, 
													(ViewGroup) findViewById (R.id.readChapterActivityLayout), 
													false) 
								   .findViewById (R.id.verseText); 
				vText.setText (node.getText () + place); 
				new AlertDialog.Builder (ChooseBook.this) 
						.setView (vText)
						.setPositiveButton (R.string.lbl_copy_text, new DialogInterface.OnClickListener () {
							@Override
							public void onClick (DialogInterface dialog, int which) {
								int selStart = vText.getSelectionStart ();
								int selEnd = vText.getSelectionEnd ();
								String text = vText.getText ().toString ();
								if (selStart != selEnd)
									text = text.substring (selStart, selEnd);
								if (Build.VERSION.SDK_INT >= 11) {
									ClipboardManager mgr =
											(ClipboardManager) getSystemService (CLIPBOARD_SERVICE);
									ClipData clip = ClipData.newPlainText (place, text);
									mgr.setPrimaryClip (clip);
								} else {
									android.text.ClipboardManager mgr =
											(android.text.ClipboardManager) getSystemService (CLIPBOARD_SERVICE);
									mgr.setText (text);
								}
								dialog.dismiss (); 
								Toast.makeText (ChooseBook.this, R.string.msg_text_copied, Toast.LENGTH_SHORT) 
										.show (); 
							}
						}) 
						.show (); 
			} 
			@Override
			public void requestChapterLoad (Bible.LoadPageDescriptor load) {
				
			}
		});
		final Activity us = this; 
		final int chapterCount = book.getChapterCount ();
		final String bookName = book.getFullname ();
		final String bookNameShort = book.getShortname ();
		final String READSPACE_ITEM_NAME = "readspace" + (++readspaceItemCount) + "";
		final BibleDbAdapter.OnLoadFinished<Bible.ReadNode> loadFinished =
				new BibleDbAdapter.OnLoadFinished<Bible.ReadNode> () {
					@Override
					public void run () {
						readChapterFragment.setReadList (result);
					}
				};
		final View.OnClickListener undoClose = new View.OnClickListener () {
			@Override public void onClick (View v) {
				FragmentManager mgr = getSupportFragmentManager ();
//				mgr.popBackStackImmediate ("remove-" + READSPACE_ITEM_NAME, 
//													  FragmentManager.POP_BACK_STACK_INCLUSIVE); 
				mgr.beginTransaction ()
						.add (R.id.reading_space, readChapterFragment, READSPACE_ITEM_NAME)
						.commit ();
				prevReadspaceItem = readChapterFragment;
				readspaceOpenCount++;
			}
		}; 
		readChapterFragment.setBook (book); 
		readChapterFragment.setOnReadchapterViewCreateListener (
					   new ReadChapterFragment.OnReadchapterViewCreateListener () {
						   @Override
						   public void onReadchapterViewCreate (View v) {
							   LinearLayout.LayoutParams layoutParams =
									   (LinearLayout.LayoutParams) v.getLayoutParams ();
							   layoutParams.height = 0;
							   layoutParams.weight = 1;
						   }
					   });
		readChapterFragment.setControlPanelAdapter (controlPanelAdapter); 
		controlPanelAdapter.setOnControlPanelInteractionListener (
						 new ControlPanelAdapter.OnControlPanelInteractionListener () {
							 @Override
							 public void onReadspaceClose () {
								 getSupportFragmentManager ()
										 .beginTransaction ()
										 .remove (readChapterFragment)
												  //.addToBackStack ("remove-" + READSPACE_ITEM_NAME) 
										 .commit ();
								 Snackbar.make (readingSpace, R.string.snack_readspace_item_closed,
													   Snackbar.LENGTH_SHORT)
										 .setAction (R.string.action_undo_close,
															undoClose)
										 .show ();
								 prevReadspaceItem = null;
								 readspaceOpenCount--;
							 }
							 @Override
							 public void onPrevChapterClick () {
								 if (loadDescriptor.getPlace ().getChapter () < 2) {
									 Toast msg =
											 Toast.makeText (us, R.string.not_allowed__ch_prev,
																	Toast.LENGTH_SHORT);
									 msg.show ();
									 return;
								 }
								 loadDescriptor.getPlace ().setChapter (loadDescriptor
																				.getPlace ().getChapter () - 1);
								 bible.verses.fetchAsync (loadDescriptor, loadFinished); 
								 chapterSelect.setSelectedChapter (loadDescriptor.getPlace ().getChapter ()); 
								 readChapterFragment.updateControlPanel ();
							 }
							 @Override
							 public void onNextChapterClick () {
								 if (loadDescriptor.getPlace ().getChapter () > chapterCount) {
									 Toast msg =
											 Toast.makeText (us, R.string.not_allowed__ch_next,
																	Toast.LENGTH_SHORT);
									 msg.show ();
									 return;
								 }
								 loadDescriptor.getPlace ().setChapter (loadDescriptor
																				.getPlace ().getChapter () + 1);
								 bible.verses.fetchAsync (loadDescriptor, loadFinished); 
								 chapterSelect.setSelectedChapter (loadDescriptor.getPlace ().getChapter ()); 
								 readChapterFragment.updateControlPanel (); 
							 }
							 @Override public String onRequestFullBookName () {
								 return bookName;
							 }
							 @Override public String onRequestShortBookName () {
								 return bookNameShort;
							 }
							 @Override public int onRequestChapterNumber () {
								 return loadDescriptor.getPlace ().getChapter ();
							 }
						 }
		); 
		if (readChapter == null) { 
			FragmentTransaction ft = getSupportFragmentManager ().beginTransaction (); 
			if (prevReadspaceItem != null) 
				ft.remove (prevReadspaceItem); 
			ft.add (R.id.reading_space, readChapterFragment, READSPACE_ITEM_NAME); 
			ft.commit (); 
		}
		bible.verses.fetchAsync (loadDescriptor, loadFinished); 
		readspaceOpenCount++; 
		prevReadspaceItem = readChapterFragment; 
	} 
	
	public static class ControlPanelAdapter 
			extends ReadChapterFragment.ControlPanelViewAdapter<ControlPanelAdapter.Holder> { 
		OnControlPanelInteractionListener onControlPanelInteractionListener = null; 
		public class Holder extends ReadChapterFragment.ControlPanelViewHolder { 
			public final ImageButton btnClose; 
			public final ImageButton btnGoPrev; 
			public final ImageButton btnGoNext; 
			public final VerticalTextView vText; 
			public final TextView vChapterNumber; 
			public Holder (View parent) { 
				super (parent); 
				btnClose = (ImageButton) parent.findViewById (R.id.btn_close_readspace); 
				btnGoPrev = (ImageButton) parent.findViewById (R.id.btn_prev_chapter); 
				btnGoNext = (ImageButton) parent.findViewById (R.id.btn_next_chapter); 
				btnClose.setOnClickListener (new View.OnClickListener () {
					@Override
					public void onClick (View v) {
						if (onControlPanelInteractionListener != null) 
							onControlPanelInteractionListener.onReadspaceClose (); 
					}
				}); 
				btnGoPrev.setOnClickListener (new View.OnClickListener () {
					@Override
					public void onClick (View v) {
						if (onControlPanelInteractionListener != null) 
							onControlPanelInteractionListener.onPrevChapterClick (); 
					}
				}); 
				btnGoNext.setOnClickListener (new View.OnClickListener () {
					@Override
					public void onClick (View v) {
						if (onControlPanelInteractionListener != null) 
							onControlPanelInteractionListener.onNextChapterClick (); 
					}
				}); 
				vText = (VerticalTextView) parent.findViewById (R.id.book_chapter_title); 
				vChapterNumber = (TextView) parent.findViewById (R.id.chapter_number); 
			} 
			public void bind () { 
				if (onControlPanelInteractionListener == null) { 
					vText.setText (""); 
					vText.setShortText (""); 
					vChapterNumber.setText (""); 
					return; 
				} 
				vText.setText (onControlPanelInteractionListener.onRequestFullBookName ()); 
				vText.setShortText (onControlPanelInteractionListener.onRequestShortBookName ()); 
				vChapterNumber.setText (onControlPanelInteractionListener.onRequestChapterNumber () + ""); 
			} 
		} 
		public Holder onCreateViewHolder (ViewGroup parent, int viewType) { 
			View vMain = LayoutInflater.from (parent.getContext ()) 
								 .inflate (R.layout.read_chapter_cmd_readspace_controls,
												  parent, false); 
			return new Holder (vMain); 
		} 
		public void onBindViewHolder (Holder holder, int position) { 
			holder.bind (); 
		} 
		public void setOnControlPanelInteractionListener 
				(OnControlPanelInteractionListener listener) { 
			onControlPanelInteractionListener = listener; 
		} 
		public interface OnControlPanelInteractionListener { 
			void onReadspaceClose (); 
			void onPrevChapterClick (); 
			void onNextChapterClick (); 
			String onRequestFullBookName (); 
			String onRequestShortBookName (); 
			int onRequestChapterNumber (); 
		} 
	} 
	
	public boolean isReadingSpaceLargeEnough () { 
		return readingSpace.getHeight () >= pixelsFromDP (380); 
	} 
	public boolean willReadingSpaceBeEnoughForAnotherItem () { 
		int willBeHeightEach = readingSpace.getHeight () / (readspaceOpenCount + 1); 
		int minRequired = (int) (160f * minReadspaceItemHeight); 
		return willBeHeightEach >= pixelsFromDP (minRequired); 
	} 
	
	@Override public boolean onSearchRequested () {
		SearchManager mgr = (SearchManager) getSystemService (Context.SEARCH_SERVICE); 
		if (mgr != null) { 
			Bundle appData = new Bundle (); 
			if (chooseBook.getSelected () != null) { 
				appData.putBundle (Bible.STATE_BOOK, 
										  chooseBook.getSelected ().saveState ()); 
				mgr.startSearch (null, false, 
										new ComponentName (this, DummyActivity.class), 
										appData, false); 
			} else {
				mgr.startSearch (null, false, 
										new ComponentName (this, ChooseBook.class), 
										appData, false); 
			} 
		} 
		return false; 
	} 
	
	public float pixelsFromDP (float dp) { 
		return TypedValue.applyDimension (TypedValue.COMPLEX_UNIT_DIP, dp, getResources ().getDisplayMetrics ()); 
	} 
}
