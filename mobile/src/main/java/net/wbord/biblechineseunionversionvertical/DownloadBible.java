package net.wbord.biblechineseunionversionvertical;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import net.wbord.verticalbible.Bible;
import net.wbord.verticalbible.BibleDbAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

public class DownloadBible extends IntentService {
	private NotificationManager notificationManager = null; 
	
	public static final String ACTION_UPDATE_PROGRESS = "update_progress"; 
	public static final String ACTION_RETURN_RESULT = "loading_done"; 
	
	public static final String KEY_TOTAL_CHAPTERS = "total_chapters"; 
	public static final String KEY_LOADED_CHAPTERS = "loaded_chapters"; 
	public static final String KEY_RESULT_CODE = "result_code"; 
	
	public static final String RESULT_OK = "OK"; 
	public static final String RESULT_BAD = "BAD"; 
	
	private int NOTIFICATION = net.wbord.verticalbible.R.string.download_tag; 
	
	private PostProgress progress = new PostProgress (); 
	class PostProgress { 
		public void post (int loaded, int total) { 
			loaded_chapters.number = loaded; 
			total_chapters.number = total; 
		} 
	} 
	
	private final IBinder myBinder = new MyBinder (); 
	
	public class MyBinder extends Binder { 
		DownloadBible getService () { 
			return DownloadBible.this; 
		}
	} 
	
	public DownloadBible () { 
		super ("net.wbord.biblechineseunionversionvertical.DownloadBible"); 
	} 
	
	@Override protected void onHandleIntent (Intent workIntent) { 
		String dataString = workIntent.getDataString (); 
		progress = new PostProgress () { 
			@Override public void post (int loaded, int total) { 
				Intent update = new Intent (); 
				update.setAction (ACTION_UPDATE_PROGRESS); 
				update.addCategory (Intent.CATEGORY_DEFAULT); 
				update.putExtra (KEY_LOADED_CHAPTERS, loaded); 
				update.putExtra (KEY_TOTAL_CHAPTERS, total); 
				sendBroadcast (update); 
			} 
		}; 
		work_build.run (); 
		Intent result = new Intent (); 
		result.setAction (ACTION_RETURN_RESULT); 
		result.addCategory (Intent.CATEGORY_DEFAULT); 
		result.putExtra (KEY_RESULT_CODE, loaded_chapters.number == total_chapters.number? 
			RESULT_OK: RESULT_BAD); 
		sendBroadcast (result); 
	} 
	
//	@Override 
//	public void onCreate () { 
//		notificationManager = (NotificationManager) getSystemService (NOTIFICATION_SERVICE); 
//		showNotification (); 
//	} 
//	
//	@Override
//	public IBinder onBind (Intent intent) {
//		return myBinder; 
//	} 
//	
//	@Override 
//	public int onStartCommand (Intent intent, int flags, int startID) { 
//		return START_NOT_STICKY; 
//	} 
//	
//	@Override 
//	public void onDestroy () { 
//		notificationManager.cancel (NOTIFICATION); 
//	} 
	
	private void showNotification () { 
		CharSequence text = getText (net.wbord.verticalbible.R.string.msg_loading); 
		PendingIntent contentIntent = 
				PendingIntent.getActivity (this, 0, 
												  new Intent (this, InitDataActivity.class), 0);
		Notification notification = new NotificationCompat.Builder (this) 
				.setSmallIcon (R.mipmap.ic_launcher) 
				.setTicker (text) 
				.setWhen (System.currentTimeMillis ()) 
				.setContentTitle (getText (R.string.app_name)) 
				.setContentText (text) 
				.setContentIntent (contentIntent) 
				.build (); 
		notificationManager.notify (NOTIFICATION, notification); 
	}
	
	MandarinDbPath dbPath = new MandarinDbPath ();
	
	BibleDbAdapter bible = new BibleDbAdapter (this);
	
	class IntHolder {
		int number = 0;
	}
	public IntHolder total_chapters = new IntHolder (); 
	public IntHolder loaded_chapters = new IntHolder (); 
	
	public int getTotalChapterCount () { return total_chapters.number; } 
	public int getLoadedChapterCount () { return loaded_chapters.number; } 
	
	public void startLoading () { 
		if (isWorking ()) return; 
		work_build.start (); 
	} 
	public boolean isWorking () { return working; } 
	
	Thread work_build = new Thread (new Runnable () {
		@Override
		public void run () {
			int mode = 0;
			int books_loaded = 0;
			int ch_loaded = 0; 
			int headers_loaded = 0;
			int verses_loaded = 0;
			int prev_chapter = 0; 
			final Bible.VersePointer lastLoaded = new Bible.VersePointer ();
			final int MODE_BIBLE = 1;
			final int MODE_CHAPTER = 2;
			final int MODE_CONTEXT = 3;
			final Vector<Bible.Book> books = new Vector<> ();
			Runnable countChapters = new Runnable () {
				@Override
				public void run () {
					// Count total chapters and loaded chapters: 
					int totalChapters = 0;
					for (int i = 0; i < books.size (); i++) {
						int chapterCount = books.get (i).getChapterCount ();
						totalChapters += chapterCount;
					}
					total_chapters.number = totalChapters; 
				}
			};
			//setStatusText ("Working ... "); 
			working = true;
//			File dir = Environment.getExternalStoragePublicDirectory (Environment.DIRECTORY_DOWNLOADS);
//			if (dir.canWrite ()) {
			bible.localOpen (dbPath.getDbName ());
			bible.clear (true); // Clear and reset to work with FTS3 extension. 
			Bible.ReadNode node;
			InputStream mainText = null;
			BufferedReader reader = null;
			try {
//					mainText = getAssets ().open (getBibleTextFilename ()); 
				mainText = getFromHTTP (dbPath.getDownloadURL ());
				reader = new BufferedReader (new InputStreamReader (mainText, "UTF-8"));
				String s;
				while ((s = reader.readLine ()) != null) {
					if (s.isEmpty ()) continue;
					if (s.charAt (0) == '*') {
						// Section: 
						String section = s.substring (1);
						switch (section) {
							case "Bible":
								mode = MODE_BIBLE;
								break;
							case "Chapter":
								mode = MODE_CHAPTER;
								break;
							case "Context":
								mode = MODE_CONTEXT;
								countChapters.run (); // Count the chapters loaded. 
								break;
							default:
						}
					} else if (s.charAt (0) == '#') continue;
					else {
						if (mode == MODE_CHAPTER) {
							String columns [] = s.split (Bible.ReadNode.DELIM_COLUMN);
							if (columns.length < 4) continue;
							Bible.Book book = new Bible.Book (columns[0], columns[1], columns[2],
																	 (books_loaded < Bible.OT_BOOK_COUNT)? Bible.IS_OT: Bible.IS_NT);
							book.setChapterCount (Integer.parseInt (columns[3])); 
							books.add (book);
						} else if (mode == MODE_CONTEXT)
							if (bible.verses.insert (node = Bible.ReadNode.parseText (s)) > 0) {
								if (node.getType ().equals (Bible.ReadNode.TYPE_VERSE)) verses_loaded++;
								else if (node.getType ().equals (Bible.ReadNode.TYPE_HEADER)) headers_loaded++;
								if (node.getPlace ().getChapter () != prev_chapter) {
									ch_loaded++; 
									prev_chapter = node.getPlace ().getChapter (); 
									progress.post (ch_loaded, total_chapters.number); 
								}
							}
					} 
				}
			} catch (IOException err) {
				Log.e ("InitData", "IO Exception");
				// TODO:  Post error message. 
			} finally {
				if (reader != null) try { reader.close (); } catch (IOException err) {}
				if (mainText != null) try { mainText.close (); } catch (IOException err) {}
			}
			// Finally insert all the books: 
			for (Bible.Book book : books)
				if (bible.books.insert (book) > 0)
					books_loaded++; 
//				adapter.copyToDir (dir);
			bible.close (); 
//			} else { 
//				// TODO:  Perhaps post error message? 
//				throw new IllegalStateException ("Cannot write to directory: " + dir.getAbsolutePath ());
//			} 
			working = false; 
		}
	});
	boolean working = false;
	
	public InputStream getFromHTTP (String the_url) {
		InputStream stream = null;
		try {
			URL url = new URL (the_url);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection ();
			connection.setReadTimeout (4000);
			connection.setConnectTimeout (4000);
			connection.setRequestMethod ("GET");
			connection.setDoInput (true);
			connection.connect ();
			int response = connection.getResponseCode ();
			if (response == 200) stream = connection.getInputStream ();
			else
				Log.e ("InitData", "Status code is not 200 (HTTP, 200 = OK); rather: " + response);
		} catch (MalformedURLException err) {
			Log.e ("InitData", "Malformed URL: " + err.toString ());
		} catch (IOException err) {
			Log.e ("InitData", "IO exception: " + err.toString ());
		}
		return stream;
	} 
}
