package net.wbord.biblechineseunionversionvertical;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;

import net.wbord.verticalbible.Bible;
import net.wbord.verticalbible.BibleDbAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

public class InitDataActivity extends AppCompatActivity { 
	public static final int ACTION_INIT_DATA = 1; 
	
	public static final int RESULT_OK = 1; 
	public static final int RESULT_BAD = 2; 
	
	Handler uiHandler = new Handler (); 
	Runnable timerTick = new Runnable () {
		@Override
		public void run () {
			// Check things: 
			if (loadService != null) { 
				int total = loadService.getTotalChapterCount (); 
				int loaded = loadService.getLoadedChapterCount (); 
				if (!loadService.isWorking ()) { 
					if (loaded >= total) { 
						setResult (RESULT_OK); 
					} else { 
						setResult (RESULT_BAD); 
					} 
					finish (); 
					return; 
				} 
				if (total != 0) 
					vPercentage.setText ((loaded * 100 / total) + "%"); 
				if (vProgress.getMax () != total) 
					vProgress.setMax (total); 
				vProgress.setProgress (loaded); 
			} 
			// Do this again in a little bit: 
			uiHandler.postDelayed (timerTick, 500); 
		}
	}; 
	
	PutProgress progress = new PutProgress (); 
	class PutProgress { 
		public void put (int loaded, int total) {
			if (total != 0) 
				vPercentage.setText ((loaded * 100 / total) + "%"); 
			if (vProgress.getMax () != total) 
				vProgress.setMax (total); 
			vProgress.setProgress (loaded); 
		} 
	} 
	
	private BroadcastReceivers myReceivers; 
	public class BroadcastReceivers { 
		UpdateReceiver updateReceiver = new UpdateReceiver (); 
		ResultReceiver resultReceiver = new ResultReceiver (); 
		public class UpdateReceiver extends BroadcastReceiver { 
			@Override public void onReceive (Context ctx, Intent intent) { 
				progress.put (intent.getIntExtra (DownloadBible.KEY_LOADED_CHAPTERS, 0), 
									 intent.getIntExtra (DownloadBible.KEY_TOTAL_CHAPTERS, 0)); 
			} 
		} 
		public class ResultReceiver extends BroadcastReceiver { 
			@Override public void onReceive (Context ctx, Intent intent) { 
				switch (intent.getStringExtra (DownloadBible.KEY_RESULT_CODE)) { 
					case DownloadBible.RESULT_OK: 
						InitDataActivity.this.setResult (RESULT_OK); 
						break; 
					default: 
						InitDataActivity.this.setResult (RESULT_BAD); 
				} 
				finish (); 
			} 
		} 
	} 
	
	ProgressBar vProgress = null; 
	TextView vPercentage = null; 
	
	private DownloadBible loadService = null; 
	private ServiceConnection mConnection = new ServiceConnection () {
		@Override
		public void onServiceConnected (ComponentName name, IBinder service) {
			loadService = ((DownloadBible.MyBinder) service).getService (); 
			loadService.startLoading (); 
		}
		@Override
		public void onServiceDisconnected (ComponentName name) {
			loadService = null; 
		}
	}; 
	
	boolean isBound = false; 
	void doBindService () { 
		bindService (new Intent (this, DownloadBible.class), mConnection, BIND_AUTO_CREATE); 
		isBound = true; 
	} 
	void doUnbindService () { 
		if (!isBound) return; 
		unbindService (mConnection); 
		isBound = false; 
	} 
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_init_data); 
		vProgress = (ProgressBar) findViewById (R.id.loadProgressBar); 
		vPercentage = (TextView) findViewById (R.id.loadPercentageText); 
		startService (new Intent (this, DownloadBible.class)); 
		myReceivers = new BroadcastReceivers ();
		IntentFilter filterUpdate = new IntentFilter (DownloadBible.ACTION_UPDATE_PROGRESS); 
		IntentFilter filterResult = new IntentFilter (DownloadBible.ACTION_RETURN_RESULT); 
		filterUpdate.addCategory (Intent.CATEGORY_DEFAULT); 
		filterResult.addCategory (Intent.CATEGORY_DEFAULT); 
		registerReceiver (myReceivers.updateReceiver, filterUpdate); 
		registerReceiver (myReceivers.resultReceiver, filterResult); 
		//uiHandler.post (timerTick); 
		//doBindService (); 
	} 
	@Override 
	protected void onDestroy () { 
		//doUnbindService (); 
		unregisterReceiver (myReceivers.updateReceiver); 
		unregisterReceiver (myReceivers.resultReceiver); 
		super.onDestroy (); 
	} 
	
	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater ().inflate (R.menu.menu_init_data, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId ();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected (item);
	}
}
