package net.wbord.biblechineseunionversionvertical;

import net.wbord.verticalbible.DbPath;

/**
 * Created by SpongeBob on 11.08.2015.
 */
public class MandarinDbPath implements DbPath { 
	public static final String BIBLE_NAME = "bible-cuv-zhhant-1"; 
	public static final String DOWNLOAD_SUFFIX = ".bc.txt"; 
	public static final String DB_SUFFIX = ".db"; 
	
	public static final String HTTP_BASE_DOWNLOAD_URL = "http://192.168.0.5/BibleText/"; 
	
	public String getDbName () { 
		return BIBLE_NAME + DB_SUFFIX; 
	} 
	public String getDownloadURL () { 
		return HTTP_BASE_DOWNLOAD_URL + BIBLE_NAME + DOWNLOAD_SUFFIX; 
	} 
}
