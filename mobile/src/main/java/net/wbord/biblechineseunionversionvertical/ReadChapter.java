package net.wbord.biblechineseunionversionvertical;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import net.wbord.verticalbible.Bible;
import net.wbord.verticalbible.BibleDbAdapter;
import net.wbord.verticalbible.ChapterSelectFragment;
import net.wbord.verticalbible.ChooseBookFragment;
import net.wbord.verticalbible.ReadChapterFragment;

public class ReadChapter extends AppCompatActivity { 
	public static final String KEY_BOOK_FULLNAME = "book_fullname"; 
	public static final String KEY_BOOK_SHORTNAME = "book_shortname"; 
	public static final String KEY_CHAPTER_COUNT = "book_chapter_count"; 
	public static final String KEY_WHETHER_OT_NT = "old_or_new_testament"; 
	
	public static final String STATE_READCHAPTER_FRAGMENT = "frag_read_chapter"; 
	
	ReadChapterFragment readChapterFragment = null; 
	
	SharedPreferences pref = null; 
	MandarinDbPath dbPath = new MandarinDbPath (); 
	BibleDbAdapter bible = new BibleDbAdapter (this); 
	
	private Bible.Book book = null; 
	private Bible.LoadPageDescriptor load = null; 
	
	private boolean isLoading = false; 
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState); 
		setContentView (R.layout.activity_read_chapter); 
		pref = PreferenceManager.getDefaultSharedPreferences (this); 
		bible.simplifyChinese = pref.getBoolean (SettingsActivity.KEY_SIMPLIFY_CHINESE, false); 
		Intent intent = getIntent (); 
		if (Intent.ACTION_SEARCH.equals (intent.getAction ())) { 
			load = new Bible.LoadPageDescriptor (); 
			Bundle appData = intent.getBundleExtra (SearchManager.APP_DATA); 
			if (appData != null) { 
				if (appData.containsKey (Bible.STATE_BOOK)) { 
					if (book == null) 
						book = new Bible.Book (); 
					book.loadState (appData.getBundle (Bible.STATE_BOOK)); 
				} 
			} 
			if (book != null) 
				load.getPlace ().book = 
						book.getSym (); 
			load.setSearchQuery (intent.getStringExtra (SearchManager.QUERY)); 
		} 
		Bundle extra = savedInstanceState; 
		if (extra == null) extra = intent.getExtras (); 
		if (extra != null) { 
			if (extra.containsKey (Bible.STATE_BOOK)) { 
				book = new Bible.Book (); 
					book.loadState (extra.getBundle (Bible.STATE_BOOK)); 
			} 
			if (extra.containsKey (Bible.STATE_LOAD) && load == null) {
				load = new Bible.LoadPageDescriptor (); 
					load.loadState (extra.getBundle (Bible.STATE_LOAD)); 
			} 
			if (extra.containsKey (STATE_READCHAPTER_FRAGMENT)) 
				readChapterFragment = (ReadChapterFragment) 
					getSupportFragmentManager ().getFragment (extra, STATE_READCHAPTER_FRAGMENT); 
		} 
		if (readChapterFragment == null) { 
			Bundle args = new Bundle (); 
				args.putBoolean (ReadChapterFragment.KEY_WHETHER_READSPACE_MODE, false); 
			readChapterFragment = (ReadChapterFragment) 
										  getSupportFragmentManager ().findFragmentById (R.id.read_chapter_fragment); 
			readChapterFragment.readArguments (args); 
			readChapterFragment.setStateLoading (); 
		} 
		readChapterFragment.setOnVerseInteractionListener (new ReadChapterFragment.OnVerseInteractionListener () {
			@Override
			public boolean onVerseClick (Bible.ReadNode node) { 
				if (load != null && !load.getSearchQuery ().isEmpty () && 
						!readChapterFragment.isFoldedTextViewVisible ()) { 
					Intent goPlaceIntent = new Intent (ReadChapter.this, ReadChapter.class);
					Bible.LoadPageDescriptor needLoad = 
							new Bible.LoadPageDescriptor (); 
					Bible.Book book = readChapterFragment.getBook (node.getPlace ()); 
						needLoad.getPlace ().set (node.getPlace ()); 
						goPlaceIntent.putExtra (Bible.STATE_BOOK, book.saveState ()); 
						goPlaceIntent.putExtra (Bible.STATE_LOAD, needLoad.saveState ()); 
					startActivity (goPlaceIntent); 
					return true; 
				} 
				return false; 
			}
			@Override
			public boolean onVerseLinkClick (Bible.ReadNode node, int linkIndex) {
				return false; 
			}
			@Override
			public void onVerseLongClick (Bible.ReadNode node) {
				Bible.Book needBook = readChapterFragment.getBook (node.getPlace ()); 
				final String place = node.getType ().equals (Bible.ReadNode.TYPE_VERSE)?
									   " " +
											   "(" +
											   ((needBook != null)? needBook: book)
													   .getShortname () +
											   node.getPlace ().getChapter () + ":" +
											   node.getPlace ().getVerse () + ")": 
									   ""; 
				final EditText vText = (EditText)
											   LayoutInflater.from (ReadChapter.this) 
						.inflate (R.layout.verse_look_edittext, 
										 (ViewGroup) findViewById (R.id.readChapterActivityLayout), 
										 false) 
						.findViewById (R.id.verseText); 
					vText.setText (node.getText () + place); 
				new AlertDialog.Builder (ReadChapter.this) 
						.setView (vText) 
						.setPositiveButton (R.string.lbl_copy_text, new DialogInterface.OnClickListener () {
							@Override
							public void onClick (DialogInterface dialog, int which) {
								int selStart = vText.getSelectionStart (); 
								int selEnd = vText.getSelectionEnd ();
								String text = vText.getText ().toString ();
								if (selStart != selEnd)
									text = text.substring (selStart, selEnd); 
								if (Build.VERSION.SDK_INT >= 11) {
									ClipboardManager mgr =
											(ClipboardManager) getSystemService (CLIPBOARD_SERVICE);
									ClipData clip = ClipData.newPlainText (place, text);
									mgr.setPrimaryClip (clip); 
								} else {
									android.text.ClipboardManager mgr = 
											(android.text.ClipboardManager) getSystemService (CLIPBOARD_SERVICE); 
									mgr.setText (text); 
								} 
								dialog.dismiss (); 
								Toast.makeText (ReadChapter.this, R.string.msg_text_copied, Toast.LENGTH_SHORT); 
							}
						}) 
						.show (); 
			} 
			@Override public void requestChapterLoad (Bible.LoadPageDescriptor desc) { 
				if (isLoading && load.equals (desc)) 
					return; // Already loading. 
				load = desc; 
				loadChapter (); 
			} 
		}); 
		if (!isLoading) 
			loadChapter (); 
	} 
	
	private void loadChapter () { 
		isLoading = true; 
		if (load.getSearchQuery ().isEmpty ()) 
			setTitle (book.getFullname () + " " + load.getPlace ().getChapter ()); 
		else 
			setTitle ("\"" + load.getSearchQuery () + "\""); 
		readChapterFragment.setBook (book); 
		readChapterFragment.setStateLoading (); 
		bible.open (dbPath.getDbName ()).verses.fetchAsync (load, 
				new BibleDbAdapter.OnLoadFinished<Bible.ReadNode> () {
					@Override
					public void run () {
						readChapterFragment.setReadList (result); 
						readChapterFragment.resetReadPage (); // Resets scroll, etc. 
						readChapterFragment.setLoadDesc (load); 
						isLoading = false; 
					}
	   }); 
		if (!load.getSearchQuery ().isEmpty ()) 
			bible.books.fetchAsync (new BibleDbAdapter.OnLoadFinished<Bible.Book> () {
			@Override
			public void run () {
				readChapterFragment.setBookList (result); 
			}
		}); 
	} 
	
	@Override public void onSaveInstanceState (Bundle outState) { 
		fillOutBundle (outState); 
		// Super?: 
		super.onSaveInstanceState (outState); 
	} 
	private void fillOutBundle (Bundle outState) {
		// Book: 
		if (book != null) 
			outState.putBundle (Bible.STATE_BOOK, book.saveState ()); 
		// Place: 
		if (load != null) 
			outState.putBundle (Bible.STATE_LOAD, load.saveState ()); 
		// Fragment: 
		if (readChapterFragment != null) 
			getSupportFragmentManager ().putFragment (outState, 
														 STATE_READCHAPTER_FRAGMENT, 
														 readChapterFragment); 
	} 
	
	@Override protected void onPause () { 
		bible.close (); 
		super.onPause (); 
	} 
	@Override protected void onResume () { 
		super.onResume (); 
		bible.open (dbPath.getDbName ()); 
		boolean simp = pref.getBoolean (SettingsActivity.KEY_SIMPLIFY_CHINESE, false); 
		if (simp != bible.simplifyChinese) { 
			bible.simplifyChinese = simp; 
			loadChapter (); 
		} 
	} 
	
	@Override protected void onDestroy () { 
		bible.close (); 
		super.onDestroy (); 
	} 


	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater ().inflate (R.menu.menu_read_chapter, menu); 
		return true;
	}

	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId ();
		
		switch (id) { 
			case R.id.settings: 
				
				return true; 
			case R.id.action_next_chapter: 
				if (navigateRelativeChapter (+1)) return true; 
				break; 
			case R.id.action_prev_chapter: 
				if (navigateRelativeChapter (-1)) return true; 
				break; 
			case android.R.id.home: 
				if (Build.VERSION.SDK_INT >= 16) { 
					Intent check1 = new Intent (this, ChooseBook.class); 
					Intent parentActivityIntent = getParentActivityIntent (); 
					if (parentActivityIntent != null && 
								parentActivityIntent.getClass () != check1.getClass ()) 
						return super.onOptionsItemSelected (item); 
				} else return super.onOptionsItemSelected (item); 
				onBackPressed (); 
				return true; 
		} 
		
		return super.onOptionsItemSelected (item); 
	} 
	
	@Override public boolean onKeyDown (int keyCode, KeyEvent event) { 
		if (keyCode == KeyEvent.KEYCODE_MENU) { 
			Intent startSettings = new Intent (this, SettingsActivity.class); 
			startActivity (startSettings); 
//			Toast.makeText (this, R.string.action_settings, Toast.LENGTH_SHORT) 
//					.show (); 
			return true; 
		} 
		return super.onKeyDown (keyCode, event); 
	} 
	
	public boolean navigateRelativeChapter (int nav_direction) { 
		return navigateRelativeChapter (nav_direction, true, false); 
	} 
	public boolean navigateRelativeChapter (int nav_direction, boolean toast_if_error, boolean intent_nav) { 
		if (!load.getSearchQuery ().isEmpty ()) return false; 
		if (load.getPlace ().getChapter () + nav_direction > book.getChapterCount ()) { 
			if (!toast_if_error) return false; 
			Toast msg = Toast.makeText (this, R.string.not_allowed__ch_next, Toast.LENGTH_SHORT); 
			msg.show (); 
			return false; 
		} 
		if (load.getPlace ().getChapter () + nav_direction <= 0) { 
			if (!toast_if_error) return false; 
			Toast msg = Toast.makeText (this, R.string.not_allowed__ch_prev, Toast.LENGTH_SHORT); 
			msg.show (); 
			return false; 
		} 
		Intent ourIntent = getIntent (); 
		if (ourIntent != null) { 
			Bundle extra = ourIntent.getExtras (); 
			if (extra == null) { 
				extra = new Bundle (); 
				fillOutBundle (extra); 
			} 
			extra.putInt (ChapterSelectFragment.KEY_CHAPTER_NUMBER, 
								 load.getPlace ().getChapter () + nav_direction); 
			extra.putInt (ReadChapterFragment.KEY_VERSE_NUMBER, 1); 
			ourIntent.replaceExtras (extra); 
		} 
		if (!intent_nav) { 
			load.getPlace ().setChapter (load.getPlace ().getChapter () + nav_direction); 
			load.getPlace ().setVerse (1); 
			loadChapter (); 
			return true; 
		} 
		finish (); 
		startActivity (ourIntent); 
		return true; 
	} 
}
