package net.wbord.biblechineseunionversionvertical;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.wbord.verticalbible.VerticalListView;

public class Test_VerticalList extends AppCompatActivity { 
	VerticalListView vList = null; 
	SimpleAdapter adapter = null; 
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_test__vertical_list); 
		vList = (VerticalListView) findViewById (R.id.test_vertical_list); 
		vList.setAdapter (adapter = new SimpleAdapter (this)); 
		adapter.setItems (new SimpleListItem [] { 
														new SimpleListItem ("ABCD"), 
														new SimpleListItem ("1"), 
														new SimpleListItem ("Hello World!"), 
														new SimpleListItem ("The Little Brown Fox"), 
														new SimpleListItem ("12345678"), 
														new SimpleListItem ("Hi There!"), 
														new SimpleListItem ("Hey!"), 
														new SimpleListItem ("Every day"), 
														new SimpleListItem ("Testing, 12!") 
		}); 
		adapter.notifyDataSetChanged (); 
	}
	
	@Override
	public boolean onCreateOptionsMenu (Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater ().inflate (R.menu.menu_test__vertical_list, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected (MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId ();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}
		
		return super.onOptionsItemSelected (item);
	}
	
	public static class SimpleAdapter extends RecyclerView.Adapter<SimpleAdapter.Holder> { 
		protected SimpleListItem items [] = null; 
		protected final Context mCtx; 
		public class Holder extends RecyclerView.ViewHolder { 
			protected final TextView vText; 
			Holder (View parent) { 
				super (parent); 
				vText = (TextView) parent.findViewById (R.id.simple_text); 
			} 
			public void bind (SimpleListItem item) { 
				vText.setText (item.getSimpleText ()); 
			} 
		} 
		public SimpleAdapter (Context ctx) { 
			mCtx = ctx; 
		} 
		@Override public Holder onCreateViewHolder (ViewGroup parent, int viewType) { 
			View vItem = LayoutInflater.from (parent.getContext ()).inflate (R.layout.test__simple_list_item, 
																					parent, false); 
			return new Holder (vItem); 
		} 
		@Override public void onBindViewHolder (Holder holder, int position) { 
			holder.bind (getItem (position)); 
		} 
		@Override public int getItemCount () { return items == null? 0: items.length; } 
		
		public void setItems (SimpleListItem list []) { items = list; } 
		public SimpleListItem getItem (int position) { return items[position]; } 
	} 
	public static class SimpleListItem { 
		public String simpleText = ""; 
		// Constructors: 
		public SimpleListItem () {} 
		public SimpleListItem (String initText) { setSimpleText (initText); } 
		// Methods: 
		public void setSimpleText (String s) { simpleText = s; } 
		public String getSimpleText () { return simpleText; } 
	} 
}
