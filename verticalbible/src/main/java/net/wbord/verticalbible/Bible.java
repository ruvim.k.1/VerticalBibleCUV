package net.wbord.verticalbible;

import android.os.Bundle;
import android.support.v4.util.Pair;

import java.util.Objects;
import java.util.Vector;

/**
 * Created by SpongeBob on 11.08.2015.
 */
public class Bible { 
	public static final int TOTAL_BOOK_COUNT = 66; 
	public static final int OT_BOOK_COUNT = 39; 
	public static final int NT_BOOK_COUNT = 27; 
	
	public static final String STATE_BOOK = "book"; 
	public static final String STATE_LOAD = "load"; 
	public static final String STATE_PTR = "point"; 
	
	public static final String IS_OT = "old_testament"; 
	public static final String IS_NT = "new_testament"; 
	
	public static class Book { 
		// Constants: 
		public static final String STATE_BOOK_SYM = "book_sym"; 
		public static final String STATE_BOOK_FULLNAME = "book_fullname"; 
		public static final String STATE_BOOK_SHORTNAME = "book_shortname"; 
		public static final String STATE_BOOK_WHICH_OTNT = "book_which_ot_nt"; 
		public static final String STATE_BOOK_CHAPTER_COUNT = "book_chapter_count"; 
		// Variables: 
		public String sym = ""; 
		public String fullname = ""; 
		public String shortname = ""; 
		public String which_ot_nt = ""; 
		public int chapterCount = 0; 
		// Temporary Variables: 
		public boolean выделить = false; 
		// Constructor: 
		public Book () {} 
		public Book (String s, String name_full, String name_short, String arg_which_ot_nt) { 
			setSym (s); 
			setFullname (name_full); 
			setShortname (name_short); 
			setWhichOtNt (arg_which_ot_nt); 
		} 
		// Setter Methods: 
		public void setSym (String s) { sym = s; } 
		public void setFullname (String name) { fullname = name; } 
		public void setShortname (String name) { shortname = name; } 
		public void setWhichOtNt (String which) { which_ot_nt = which; } 
		public void setChapterCount (int count) { chapterCount = count; } 
		// Getter Methods: 
		public String getSym () { return sym; } 
		public String getFullname () { return fullname; } 
		public String getShortname () { return shortname; } 
		public String getWhichOtNt () { return which_ot_nt; } 
		public int getChapterCount () { return chapterCount; } 
		// Save and Restore State: 
		public void saveState (Bundle outState) { 
			outState.putString (STATE_BOOK_SYM, getSym ()); 
			outState.putString (STATE_BOOK_FULLNAME, getFullname ()); 
			outState.putString (STATE_BOOK_SHORTNAME, getShortname ()); 
			outState.putString (STATE_BOOK_WHICH_OTNT, getWhichOtNt ()); 
			outState.putInt (STATE_BOOK_CHAPTER_COUNT, getChapterCount ()); 
		} 
		public void loadState (Bundle inState) { 
			setSym (inState.getString (STATE_BOOK_SYM)); 
			setFullname (inState.getString (STATE_BOOK_FULLNAME)); 
			setShortname (inState.getString (STATE_BOOK_SHORTNAME)); 
			setWhichOtNt (inState.getString (STATE_BOOK_WHICH_OTNT)); 
			setChapterCount (inState.getInt (STATE_BOOK_CHAPTER_COUNT)); 
		} 
		public Bundle saveState () { 
			Bundle state = new Bundle (); 
			saveState (state); 
			return state; 
		} 
		// Compare: 
		@Override public boolean equals (Object other) { 
			if (this == other) return  true; 
			if (other == null) return false; 
			if (!(other instanceof Book)) return false; 
			Book compareTo = (Book) other; 
			if (!getSym ().equals (compareTo.getSym ())) return false; 
			if (!getFullname ().equals (compareTo.getFullname ())) return false; 
			if (!getShortname ().equals (compareTo.getShortname ())) return false; 
			if (!getWhichOtNt ().equals (compareTo.getWhichOtNt ())) return false; 
			return getChapterCount () == compareTo.getChapterCount (); 
		} 
	} 
	
	public static class LoadPageDescriptor { 
		// Constants: 
		public static final String STATE_PLACE = "place"; 
		public static final String STATE_SEARCH = "search_query"; 
		// Variables: 
		VersePointer place = new VersePointer (); 
		String searchQuery = ""; 
		// Methods: 
		public void setPlace (VersePointer p) { place = p; } 
		public void setSearchQuery (String query) { searchQuery = query; } 
		public VersePointer getPlace () { return place; } 
		public String getSearchQuery () { return searchQuery; } 
		// Save and Restore State: 
		public void saveState (Bundle outState) { 
			outState.putBundle (STATE_PLACE, place.saveState ()); 
			outState.putString (STATE_SEARCH, getSearchQuery ()); 
		} 
		public void loadState (Bundle inState) { 
			place.loadState (inState.getBundle (STATE_PLACE)); 
			setSearchQuery (inState.getString (STATE_SEARCH)); 
		} 
		public Bundle saveState () { 
			Bundle state = new Bundle (); 
			saveState (state); 
			return state; 
		} 
		@Override public boolean equals (Object objOther) { 
			if (objOther == null) return false; 
			if (!(objOther instanceof LoadPageDescriptor)) return false; 
			LoadPageDescriptor other = (LoadPageDescriptor) objOther; 
			if (!getPlace ().equals (other.getPlace ())) return false; 
			if (!getSearchQuery ().equals (other.getSearchQuery ())) return false; 
			return true; 
		} 
	} 
	
	public static abstract class ReadNode { 
		// Constants: 
		public static final String TYPE_CHAPTER = "chapter_title"; 
		public static final String TYPE_HEADER = "header"; 
		public static final String TYPE_VERSE = "verse"; 
		public static final String DELIM_COLUMN = "\\|"; 
		public static final String DELIM_FOLD_STAR = "*"; 
		// Variables: 
		VersePointer place = new VersePointer (); 
		String text = ""; 
		String textFolded = ""; 
		String foldedList [] = new String [0]; 
		int foldedSymbolPositions [] = new int [0]; 
		int foundSymbolPositions [] = new int [0]; 
		// Methods: 
		public abstract String getType (); 
		public void setPlace (VersePointer p) { place = p; } 
		public void setText (String s) { text = s; parseFoldedTexts (); } 
		public VersePointer getPlace () { return place; } 
		public String getText () { return text; } 
		public String getText (boolean whether_fold_parenthetical) { 
			return whether_fold_parenthetical? textFolded: text; } 
		public int getFoldedSymbolStartPosition (int index) { return foldedSymbolPositions[2 * index]; } 
		public int getFoldedSymbolEndPosition (int index) { return foldedSymbolPositions[2 * index + 1]; } 
		public String getFoldedText (int index) { return foldedList[index]; } 
		public int getFoldedCount () { return foldedList.length; } 
		protected void parseFoldedTexts () { 
			String arr [] = text.split ("（|\\("); 
			int i; 
			int c = 0; 
			int lengthRead = arr[0].length (); 
			for (i = 1; i < arr.length; i++) 
				if (arr[i].contains ("）") || arr[i].contains (")")) c++; 
			textFolded = arr[0]; 
			foldedList = new String [c]; 
			foldedSymbolPositions = new int [2 * c]; 
			for (i = 1, c = 0; i < arr.length; i++) { 
				String txt [] = arr[i].split ("）|\\)"); 
				if (txt.length < 2 && !arr[i].contains ("）") && !arr[i].contains (")")) { 
					textFolded += text.charAt (lengthRead) + arr[i]; 
					lengthRead += 1 + arr[i].length (); 
					continue; 
				} 
				foldedSymbolPositions[2 * c] = textFolded.length (); 
				foldedSymbolPositions[2 * c + 1] = 
						textFolded.length () + DELIM_FOLD_STAR.length (); 
				textFolded += DELIM_FOLD_STAR + (txt.length > 1? txt[1]: ""); 
				foldedList[c] = txt[0]; 
				lengthRead += 1 + arr[i].length (); 
				c++; 
			} 
		} 
		public int getFoundSymbolStartPosition (int index) { return foundSymbolPositions[2 * index]; } 
		public int getFoundSymbolEndPosition (int index) { return foundSymbolPositions[2 * index + 1]; } 
		public int getFoundCount () { return foundSymbolPositions.length / 2; } 
		public void findWordInstances (String [] words) {
			Vector<Pair<Integer,Integer>> positions = new Vector<> (); 
			int i, c; 
			i = -1; 
			for (c = 0; c < words.length; c++, i = -1) 
				if (!words[c].isEmpty ()) 
					while ((i = textFolded.indexOf (words[c], i + 1)) >= 0) 
						positions.add (new Pair<> (i, i + words[c].length ())); 
			foundSymbolPositions = new int [2 * positions.size ()]; 
			for (i = 0; i < positions.size (); i++) { 
				foundSymbolPositions[2 * i + 0] = positions.get (i).first; 
				foundSymbolPositions[2 * i + 1] = positions.get (i).second; 
			} 
		} 
		// Factory: 
		public static ReadNode parseText (String text) { 
			String columns [] = text.split (DELIM_COLUMN); 
			ReadNode node = null; 
			if (columns.length < 5) return null; 
			if (columns[3].equals ("*")) node = new Header (); 
			else if (columns[3].isEmpty ()) node = new Verse (); 
			else return null; 
			VersePointer p = node.getPlace (); 
			p.set (columns[0], Integer.parseInt (columns[1]), Integer.parseInt (columns[2])); 
			node.setText (columns[4]); 
			return node; 
		} 
	} 
	
	public static class Chapter extends ReadNode { 
		public Chapter () {} 
		public Chapter (VersePointer where) { 
			if (where != null) 
				setPlace (where); 
		} 
		@Override public String getType () { return TYPE_CHAPTER; } 
	} 
	public static class Header extends ReadNode { 
		@Override public String getType () { return TYPE_HEADER; } 
	} 
	public static class Verse extends ReadNode { 
		@Override public String getType () { return TYPE_VERSE; } 
	} 
	
	public static class VersePointer { 
		// Constants: 
		public static final String STATE_BOOK = "ptr_book"; 
		public static final String STATE_CHAPTER = "ptr_chapter"; 
		public static final String STATE_VERSE = "ptr_verse"; 
		// Variables: 
		public String book = ""; 
		public int chapter = 0; 
		public int verse = 0; 
		// Methods: 
		public void setBook (String b) { book = b; } 
		public void setChapter (int n) { chapter = n + 1; } 
		public void setVerse (int n) { verse = n + 1; } 
		public void set (String the_book, int the_chapter, int the_verse) { 
			setBook (the_book); 
			setChapter (the_chapter); 
			setVerse (the_verse); 
		} 
		public void set (VersePointer from) { set (from.book, from.getChapter (), from.getVerse ()); } 
		public void clear () { book = ""; chapter = 0; verse = 0; } 
		public int getChapter () { return chapter - 1; } 
		public int getVerse () { return verse - 1; } 
		@Override public String toString () { return book + " " + getChapter () + ":" + getVerse (); } 
		public void saveState (Bundle outState) { 
			outState.putString (STATE_BOOK, book); 
			outState.putInt (STATE_CHAPTER, getChapter ()); 
			outState.putInt (STATE_VERSE, getVerse ()); 
		} 
		public void loadState (Bundle inState) { 
			book = inState.getString (STATE_BOOK); 
			setChapter (inState.getInt (STATE_CHAPTER)); 
			setVerse (inState.getInt (STATE_VERSE)); 
		} 
		public Bundle saveState () { 
			Bundle state = new Bundle (); 
			saveState (state); 
			return state; 
		} 
		@Override public boolean equals (Object objOther) { 
			if (objOther == null) return false; 
			if (!(objOther instanceof VersePointer)) return false; 
			VersePointer other = (VersePointer) objOther; 
			if (!book.equals (other.book)) return false; 
			if (getChapter () != other.getChapter ()) return false; 
			if (getVerse () != other.getVerse ()) return false; 
			return true; 
		} 
	} 
	
} 
