package net.wbord.verticalbible;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.database.DatabaseUtilsCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.spreada.utils.chinese.ZHConverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Vector;

import javax.security.auth.callback.Callback;

/**
 * Created by SpongeBob on 11.08.2015.
 */
public class BibleDbAdapter implements DbContext { 
	// Database Name and Version: 
	public static String DB_NAME = "bible"; 
	public static final int DB_VER = 1; 
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"; 
	// Flags and Settings: 
	public boolean simplifyChinese = false; 
	// Access: 
	public BookTable books = new BookTable (this); 
	public VerseTable verses = new VerseTable (this); 
	// Log Tag: 
	public static final String LOG_TAG = "BibleDB"; 
	// Private Variables: 
	SQLiteOpenHelper mDbHelper = null; 
	SQLiteDatabase mDb = null; 
	ZHConverter zhConverter = ZHConverter.getInstance (ZHConverter.SIMPLIFIED); 
	Context mCtx = null; 
	// Classes: 
	public static abstract class OnLoadFinished<TYPE> implements Runnable { 
		public TYPE result []; 
	} 
	public static class TrackFTS3 { 
		public static final String NAME = "TrackFTS3"; 
		public static class Keys { 
			public static final String ID = "_id"; 
			public static final String NAME = "name"; 
		} 
		public static void ensureExists (SQLiteDatabase db) { 
			db.execSQL ("CREATE TABLE IF NOT EXISTS " + NAME + " (" +
								Keys.ID + " integer primary key autoincrement, " +
								Keys.NAME + " text" + 
			");"); 
		} 
		public static boolean check (SQLiteDatabase db, String name) { 
			Cursor c = db.query (NAME, new String [] { Keys.NAME }, Keys.NAME + " = ?", 
										new String [] { name }, null, null, null); 
			int count = c.getCount (); 
			c.close (); 
			return count > 0; 
		} 
		public static boolean insert (SQLiteDatabase db, String name) { 
			ContentValues vals = new ContentValues (); 
			vals.put (Keys.NAME, name); 
			return db.insert (NAME, null, vals) > 0; 
		} 
	} 
	public static class BookTable { 
		// Constants: 
		public static final String NAME = "books"; 
		public static class Keys { 
			public static final String ID = "_id"; 
			public static final String SYMBOL = "book_symbol"; 
			public static final String FULLNAME = "fullname"; 
			public static final String SHORTNAME = "shortname"; 
			public static final String CHAPTER_COUNT = "chapter_count"; 
			public static final String WHICH_OT_NT = "old_new_testament"; 
		} 
		// Variables: 
		private final DbContext dbContext; 
		// Methods: 
		BookTable (DbContext ctx) { dbContext = ctx; }
		public static void db_create_fts3 (SQLiteDatabase db) { 
			db.execSQL ("CREATE VIRTUAL TABLE " + NAME + " USING fts3 (" + 
								Keys.ID	 + " integer primary key autoincrement, " + 
								Keys.SYMBOL + " text not null, " + 
								Keys.FULLNAME + " text not null, " + 
								Keys.SHORTNAME + " text not null, " + 
								Keys.CHAPTER_COUNT + " integer, " + 
								Keys.WHICH_OT_NT + " text" + 
			");"); 
		} 
		public static void db_create (SQLiteDatabase db) { 
			db.execSQL ("create table " + NAME + " (" + 
								Keys.ID	 + " integer primary key autoincrement, " + 
								Keys.SYMBOL + " text not null, " + 
								Keys.FULLNAME + " text not null, " + 
								Keys.SHORTNAME + " text not null, " + 
								Keys.CHAPTER_COUNT + " integer, " + 
								Keys.WHICH_OT_NT + " text" + 
			");"); 
		} 
		public static void db_delete (SQLiteDatabase db) { 
			db.execSQL ("DROP TABLE IF EXISTS " + NAME); 
		} 
		public static void db_upgrade (SQLiteDatabase db, int from) { 
			db_delete (db); 
			db_create (db); 
		} 
		// Public Access: 
		public long insert (Bible.Book book) { 
			dbContext.failIfNoDB ();
			ContentValues vals = new ContentValues (); 
			vals.put (Keys.SYMBOL, book.getSym ()); 
			vals.put (Keys.FULLNAME, book.getFullname ()); 
			vals.put (Keys.SHORTNAME, book.getShortname ()); 
			vals.put (Keys.CHAPTER_COUNT, book.getChapterCount ()); 
			vals.put (Keys.WHICH_OT_NT, book.getWhichOtNt ()); 
			return dbContext.getDB ().insert (NAME, null, vals); 
		} 
		public Cursor fetch () { 
			dbContext.failIfNoDB (); 
			return dbContext.getDB ().query (NAME, new String [] { 
				Keys.ID, Keys.SYMBOL, Keys.FULLNAME, Keys.SHORTNAME, Keys.CHAPTER_COUNT, Keys.WHICH_OT_NT 
			}, null, null, null, null, null); 
		} 
		public void fetchAsync (final OnLoadFinished<Bible.Book> callback) { 
			Thread thread = new Thread (new Runnable () {
				@Override
				public void run () {
					Cursor c = fetch (); 
					callback.result = new Bible.Book [c.getCount ()]; 
					c.moveToFirst (); 
					int i = 0; 
					while (!c.isAfterLast ()) { 
						Bible.Book book = 
								callback.result[i] = 
										new Bible.Book ( 
												dbContext.applyModifiers (c.getString (c.getColumnIndex (Keys.SYMBOL))), 
												dbContext.applyModifiers (c.getString (c.getColumnIndex (Keys.FULLNAME))), 
												dbContext.applyModifiers (c.getString (c.getColumnIndex (Keys.SHORTNAME))), 
															   c.getString (c.getColumnIndex (Keys.WHICH_OT_NT)) 
										); 
						book.setChapterCount (c.getInt (c.getColumnIndex (Keys.CHAPTER_COUNT))); 
						c.moveToNext (); 
						i++; 
					} 
					c.close (); 
					((Activity) dbContext.getAndroidCtx ()).runOnUiThread (callback); 
				}
			}); 
			thread.start (); 
		} 
		public boolean convert2fts3 () {
			String tmp_name = "tmp_" + NAME; 
			dbContext.getDB ().execSQL ("ALTER TABLE " + NAME + " RENAME TO " + tmp_name); 
			db_create_fts3 (dbContext.getDB ()); 
			dbContext.getDB ().execSQL ("INSERT INTO " + NAME + " SELECT * FROM " + tmp_name); 
			dbContext.getDB ().execSQL ("DROP TABLE IF EXISTS " + tmp_name); 
			return TrackFTS3.insert (dbContext.getDB (), NAME); 
		} 
		public boolean isFTS3 () { 
			dbContext.failIfNoDB (); 
			TrackFTS3.ensureExists (dbContext.getDB ()); 
			return TrackFTS3.check (dbContext.getDB (), NAME); 
		} 
	} 
	public static class VerseTable { 
		// Constants: 
		public static final String NAME = "verses"; 
		public static class Keys { 
			public static final String ID = "_id"; 
			public static final String BOOK = "book"; 
			public static final String CHAPTER = "chapter"; 
			public static final String VERSE = "verse"; 
			public static final String TYPE = "type"; 
			public static final String TEXT = "text"; 
		} 
		// Variables: 
		private final DbContext dbContext; 
		private long lastKnownRowCount = 0; 
		// Methods: 
		VerseTable (DbContext ctx) { dbContext = ctx; } 
		public static void db_create_fts3 (SQLiteDatabase db) { 
			db.execSQL ("CREATE VIRTUAL TABLE " + NAME + " USING fts3 (" + 
								Keys.ID + " integer primary key autoincrement, " + 
								Keys.BOOK + " text not null, " + 
								Keys.CHAPTER + " integer not null, " + 
								Keys.VERSE + " integer not null, " + 
								Keys.TYPE + " text, " + 
								Keys.TEXT + " text" + 
			");"); 
		} 
		public static void db_create (SQLiteDatabase db) { 
			db.execSQL ("create table " + NAME + " (" +
								Keys.ID + " integer primary key autoincrement, " + 
								Keys.BOOK + " text not null, " + 
								Keys.CHAPTER + " integer not null, " + 
								Keys.VERSE + " integer not null, " + 
								Keys.TYPE + " text, " + 
								Keys.TEXT + " text" + 
			");"); 
		} 
		public static void db_delete (SQLiteDatabase db) { 
			db.execSQL ("DROP TABLE IF EXISTS " + NAME); 
		} 
		public static void db_upgrade (SQLiteDatabase db, int from) { 
			db_delete (db); 
			db_create (db); 
		} 
		// Public Access: 
		public long insert (Bible.ReadNode node) { 
			if (node == null) return 0; 
			dbContext.failIfNoDB (); 
			ContentValues vals = new ContentValues (); 
			vals.put (Keys.BOOK, node.getPlace ().book); 
			vals.put (Keys.CHAPTER, node.getPlace ().getChapter ()); 
			vals.put (Keys.VERSE, node.getPlace ().getVerse ()); 
			vals.put (Keys.TYPE, node.getType ()); 
			vals.put (Keys.TEXT, node.getText ()); 
			return dbContext.getDB ().insert (NAME, null, vals); 
		} 
		public Cursor fetchRandom () { 
			dbContext.failIfNoDB (); 
			if (lastKnownRowCount == 0) 
				// Count rows: 
				lastKnownRowCount = 
						DatabaseUtils.queryNumEntries (dbContext.getDB (), NAME); 
			long randomID = 1 + (long) Math.floor (lastKnownRowCount * Math.random ()); 
			if (randomID > lastKnownRowCount) // Shouldn't overflow, but 
				randomID = lastKnownRowCount; // check just in case. 
			return dbContext.getDB ().query (NAME, new String [] {
				Keys.ID, Keys.BOOK, Keys.CHAPTER, Keys.VERSE, Keys.TYPE, Keys.TEXT 
			}, Keys.ID + " = " + randomID, null, null, null, null); 
		} 
		public Cursor fetchRandom (Bible.Book fromBook) { 
			dbContext.failIfNoDB (); 
			long startID; 
			long idCount; 
			// Count rows: 
				Cursor c = dbContext.getDB ().rawQuery ("SELECT MIN(" + Keys.ID + 
																"), COUNT(*) FROM " + NAME + " WHERE " + 
																Keys.BOOK + " = ?", 
															   new String [] { fromBook.getSym () }); 
				c.moveToFirst (); 
				startID = c.getLong (0); 
				idCount = c.getLong (1); 
				c.close (); 
			// Fetch random: 
			long randomID = startID + (long) Math.floor (idCount * Math.random ()); 
			return dbContext.getDB ().query (NAME, new String [] { 
				Keys.ID, Keys.BOOK, Keys.CHAPTER, Keys.VERSE, Keys.TYPE, Keys.TEXT 
			}, Keys.ID + " = " + randomID, null, null, null, null); 
		} 
		public Cursor fetch (String book, int chapter) { 
			dbContext.failIfNoDB (); 
			return dbContext.getDB ().query (NAME, new String [] { 
				Keys.ID, Keys.BOOK, Keys.CHAPTER, Keys.VERSE, Keys.TYPE, Keys.TEXT 
			}, Keys.BOOK + " = '" + book + "' AND " + Keys.CHAPTER + " = " + chapter, null, null, null, null); 
		} 
		public Cursor fetch (Bible.LoadPageDescriptor loadDescriptor) { 
			dbContext.failIfNoDB (); 
			if (loadDescriptor.getSearchQuery ().isEmpty ()) 
				return fetch (loadDescriptor.getPlace ().book, 
									 loadDescriptor.getPlace ().getChapter ()); 
			String word = dbContext.getSqliteSearchMatch (loadDescriptor.getSearchQuery ()); 
			String pattern = NAME + " MATCH ?"; 
			String whereClause = pattern; 
			String alsoWhere = ""; 
			String need [] = new String [] { 
												   Keys.ID, Keys.BOOK, Keys.CHAPTER, Keys.VERSE, Keys.TYPE, Keys.TEXT 
			}; 
			String whereArgs []; 
			if (loadDescriptor.getPlace () != null && 
						!loadDescriptor.getPlace ().book.isEmpty ()) { 
				alsoWhere = Keys.BOOK + " = ? AND "; 
				whereArgs = new String [] { 
												  loadDescriptor.getPlace ().book, 
												  word 
				}; 
			} else 
				whereArgs = new String [] {
						word 
				}; 
			Cursor c = null; 
			boolean fts3 = isFTS3 (); 
			if (fts3) 
				c = dbContext.getDB ().query (NAME, need, alsoWhere + whereClause, whereArgs, null, null, null); 
			if (c == null || c.getCount () < 1) { 
				// No results. Close: 
				if (c != null) 
					c.close (); 
				// Then try again using LIKE: 
				whereClause = Keys.TEXT + " LIKE ?"; 
				whereArgs[whereArgs.length - 1] = "%" + word + "%"; 
				c = dbContext.getDB ().query (NAME, need, alsoWhere + whereClause, 
													 whereArgs, 
													 null, null, null); 
			} 
			return c; 
		} 
		public void fetchRandomAsync (final OnLoadFinished<Bible.ReadNode> callback) { 
			Thread thread = new Thread (new Runnable () {
				@Override
				public void run () {
					Cursor c = fetchRandom (); 
					doneLoading (c, callback, null); 
				}
			}); 
			thread.start (); 
		} 
		public void fetchRandomAsync (final Bible.Book fromBook, 
									  final OnLoadFinished<Bible.ReadNode> callback) { 
			Thread thread = new Thread (new Runnable () {
				@Override
				public void run () {
					Cursor c = fetchRandom (fromBook); 
					doneLoading (c, callback, null); 
				}
			}); 
			thread.start (); 
		} 
		public void fetchAsync (final String book, final int chapter, final OnLoadFinished<Bible.ReadNode> callback) { 
			Thread thread = new Thread (new Runnable () {
				@Override
				public void run () {
					Cursor c = fetch (book, chapter); 
					doneLoading (c, callback, null); 
				}
			}); 
			thread.start (); 
		} 
		public void fetchAsync (final Bible.LoadPageDescriptor loadDescriptor, 
								final OnLoadFinished<Bible.ReadNode> callback) { 
			Thread thread = new Thread (new Runnable () {
				@Override
				public void run () {
					String [] words = 
							new String [] { dbContext.getSqliteSearchMatch (loadDescriptor.getSearchQuery ()) }; 
					Cursor c = fetch (loadDescriptor); 
					doneLoading (c, callback, words); 
				}
			}); 
			thread.start (); 
		} 
		private void doneLoading (Cursor c, OnLoadFinished<Bible.ReadNode> callback, 
								  String words []) {
			Bible.ReadNode node;
			int i = 0;
			if (c.getCount () < 0) {
				throw new Error ("Could not load SQLite data");
			}
			callback.result = new Bible.ReadNode [c.getCount ()];
			c.moveToFirst ();
			while (!c.isAfterLast ()) {
				String type = c.getString (c.getColumnIndex (Keys.TYPE));
				switch (type) {
					case Bible.ReadNode.TYPE_HEADER:
						node = new Bible.Header ();
						break;
					case Bible.ReadNode.TYPE_VERSE:
						node = new Bible.Verse ();
						break;
					default:
						continue;
				}
				callback.result[i] = node;
				node.getPlace ().set (c.getString (c.getColumnIndex (Keys.BOOK)),
											 c.getInt (c.getColumnIndex (Keys.CHAPTER)),
											 c.getInt (c.getColumnIndex (Keys.VERSE)));
				node.setText (dbContext.applyModifiers (c.getString (c.getColumnIndex (Keys.TEXT)))); 
				if (words != null) 
					node.findWordInstances (words); 
				// Continue: 
				c.moveToNext ();
				i++;
			}
			c.close ();
			((Activity) dbContext.getAndroidCtx ()).runOnUiThread (callback);
		} 
		public boolean convert2fts3 () { 
			String tmp_name = "tmp_" + NAME; 
			dbContext.getDB ().execSQL ("ALTER TABLE " + NAME + " RENAME TO " + tmp_name); 
			db_create_fts3 (dbContext.getDB ()); 
			dbContext.getDB ().execSQL ("INSERT INTO " + NAME + " SELECT * FROM " + tmp_name); 
			dbContext.getDB ().execSQL ("DROP TABLE IF EXISTS " + tmp_name); 
			return TrackFTS3.insert (dbContext.getDB (), NAME); 
		} 
		public boolean isFTS3 () { 
			dbContext.failIfNoDB (); 
			return TrackFTS3.check (dbContext.getDB (), NAME); 
		} 
	} 
	public static class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.ViewHolder> 
			implements VerticalListView.ScaleableAdapter { 
		Bible.Book books [] = null; 
		ListItemClicked listener = null; 
		final Context mCtx; 
		final int layoutID; 
		float fontScale = 1; 
		public class ViewHolder extends RecyclerView.ViewHolder { 
			final View theParent; 
			final VerticalTextView vBookName; 
			public ViewHolder (View parent) { 
				super (parent); 
				parent.setClickable (true); 
				parent.setOnClickListener (new View.OnClickListener () {
					@Override
					public void onClick (View v) {
						if (listener != null) listener.onBookClicked (v); 
					}
				}); 
				theParent = parent; 
				vBookName = (VerticalTextView) parent.findViewById (R.id.book_name); 
			} 
			public void bind (Bible.Book book) { 
				vBookName.setFontScale (fontScale); 
				vBookName.setShortText (book.getShortname ()); 
				vBookName.setText (book.getFullname ()); 
				if (book.выделить) vBookName.setBackgroundResource (R.color.highlighted_text_material_dark); 
				else vBookName.setBackgroundColor (Color.TRANSPARENT); 
			} 
		} 
		public BookListAdapter (Context ctx, int layout_id) { 
			mCtx = ctx; 
			layoutID = layout_id; 
		} 
		
		@Override public ViewHolder onCreateViewHolder (ViewGroup parent, int viewType) { 
			View vItem = LayoutInflater.from (parent.getContext ()).inflate (layoutID, parent, false); 
			return new ViewHolder (vItem); 
		} 
		@Override public void onBindViewHolder (ViewHolder holder, int position) { 
			holder.bind (getItem (position)); 
		} 
		@Override public int getItemCount () { return books.length; } 
		
		public Bible.Book getItem (int position) { return books[position]; } 
		public void setBooks (Bible.Book list []) { books = list; } 
		public void setBookSelectListener (ListItemClicked l) { listener = l; } 
		
		@Override public void setScale (float scale) { fontScale = scale; notifyDataSetChanged (); } 
		
		public void setFontScale (float s) { setScale (s); } 
		public float getFontScale () { return fontScale; } 
		
		public interface ListItemClicked { 
			void onBookClicked (View v); 
		} 
	} 
	public static class ReadNodeListAdapter extends RecyclerView.Adapter<ReadNodeListAdapter.Holder> 
				implements VerticalListView.ScaleableAdapter { 
		HashMap<String,Bible.Book> books = new HashMap<> (); 
		Bible.ReadNode readNodes [] = null; 
		ReadNodeInteractionListener listener = null; 
		
		Vector<Integer> items = new Vector<> (); 
		Vector<Bible.Chapter> labels = new Vector<> (); 
		
		float fontScale = 1; 
		boolean foldParentheticalText = true; 
		
		public static final int VIEW_TYPE_HEADER = 1; 
		public static final int VIEW_TYPE_VERSE = 2; 
		public static final int VIEW_TYPE_CHAPTER = 3; 
		
		final Context mCtx; 
		
		public abstract class Holder extends RecyclerView.ViewHolder { 
			protected final VerticalTextView vText; 
			protected final GestureDetector gestureDetector; 
			protected final VerticalTextView.Highlight.Style foundHighlightStyle = 
					new VerticalTextView.Highlight.Style (Color.YELLOW, Color.RED); 
			public Holder (final View parent) { 
				super (parent); 
				gestureDetector = new GestureDetector (parent.getContext (), new GestureDetector.OnGestureListener () {
					@Override
					public boolean onDown (MotionEvent e) {
						return true; 
					} 
					@Override
					public void onShowPress (MotionEvent e) { 
					} 
					@Override
					public boolean onSingleTapUp (MotionEvent e) { 
						if (listener == null) return false; 
						int linkIndex = 
								vText.whichLinkIsPoint ((int) e.getX (), 
															   (int) e.getY (), 
															   (int) TypedValue.applyDimension (TypedValue.COMPLEX_UNIT_DIP, 
																	  24, 
																	  parent.getContext ().getResources ().getDisplayMetrics ())); 
						if (linkIndex < 0) 
							listener.onVerseViewClick (parent); 
						else 
							listener.onVerseViewLinkClick (parent, linkIndex); 
						return true; 
					} 
					@Override
					public boolean onScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
						return false;
					} 
					@Override
					public void onLongPress (MotionEvent e) { 
						if (listener != null) 
							listener.onVerseViewLongClick (parent); 
					} 
					@Override
					public boolean onFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
						return false;
					}
				}); 
				vText = (VerticalTextView) parent.findViewById (R.id.read_node_text); 
				vText.setOnTouchListener (new View.OnTouchListener () {
					@Override
					public boolean onTouch (View v, MotionEvent event) {
						return gestureDetector.onTouchEvent (event); 
					}
				}); 
			} 
			public void bind (Bible.ReadNode node) { 
				vText.setFontScale (fontScale); 
				vText.setText (node.getText (true)); 
				int count = node.getFoldedCount (); 
				for (int i = 0; i < count; i++) 
					vText.addLink (node.getFoldedSymbolStartPosition (i), 
										  node.getFoldedSymbolEndPosition (i)); 
				count = node.getFoundCount (); 
				for (int i = 0; i < count; i++) 
					vText.addHighlight (new VerticalTextView.Highlight (node.getFoundSymbolStartPosition (i), 
																			   node.getFoundSymbolEndPosition (i), 
																			   foundHighlightStyle)); 
			} 
		} 
		public class HeaderHolder extends Holder { 
			public HeaderHolder (View parent) { super (parent); }
			@Override public void bind (Bible.ReadNode node) { 
				 super.bind (node); 
			} 
		} 
		public class VerseHolder extends Holder {
			protected final TextView vNumber; 
			protected final TextView vDebug; 
			public VerseHolder (View parent) { 
				super (parent);
				vNumber = (TextView) parent.findViewById (R.id.verse_number); 
				vDebug = (TextView) parent.findViewById (R.id.verse_debug); 
			} 
			@Override public void bind (Bible.ReadNode node) {
				super.bind (node); 
				vNumber.setText ("" + node.getPlace ().getVerse ()); 
//				vDebug.setText ("Meas: (" + vText.getMeasuredWidth () + " x " + 
//										vText.getMeasuredHeight () + ")" + 
//				"\nActual: (" + vText.getWidth () + " x " + vText.getHeight () + ")"); 
			} 
		} 
		public class ChapterHolder extends Holder { 
			public ChapterHolder (View parent) { super (parent); } 
			@Override public void bind (Bible.ReadNode node) { 
				super.bind (node); 
				if (!books.containsKey (node.getPlace ().book)) 
					return; 
				vText.setText (books.get (node.getPlace ().book).getFullname () + 
									   node.getPlace ().getChapter ()); 
			} 
		} 
		
		public ReadNodeListAdapter (Context ctx) { 
			mCtx = ctx; 
		} 
		
		@Override public Holder onCreateViewHolder (ViewGroup parent, int viewType) { 
			View vItem; 
			switch (viewType) { 
				case VIEW_TYPE_HEADER: 
					vItem = LayoutInflater.from (parent.getContext ()).inflate (R.layout.verse_read_list_header_item, 
																					   parent, false); 
					return new HeaderHolder (vItem); 
				case VIEW_TYPE_VERSE: 
					vItem = LayoutInflater.from (parent.getContext ()).inflate (R.layout.verse_read_list_verse_item, 
																					   parent, false); 
					return new VerseHolder (vItem); 
				case VIEW_TYPE_CHAPTER: 
					vItem = LayoutInflater.from (parent.getContext ()).inflate (R.layout.verse_read_list_chapter_item, 
																					   parent, false); 
					return new ChapterHolder (vItem); 
				default: 
					return null; 
			} 
		} 
		@Override public void onBindViewHolder (Holder holder, int position) {
			holder.bind (getItem (position)); 
		} 
		@Override public int getItemCount () { 
			return (readNodes == null? 0: readNodes.length) + items.size (); 
		} 
		
		private void setupItems () {
			int prevChapter = 0; 
			String prevBook = ""; 
			items.clear (); 
			labels.clear (); 
			if (readNodes == null) 
				return; 
			for (int i = 0; i < readNodes.length; i++) { 
				Bible.ReadNode node = readNodes[i]; 
				if (prevChapter == node.getPlace ().getChapter () &&
							prevBook.equals (node.getPlace ().book)) continue; 
				prevChapter = node.getPlace ().getChapter (); 
				prevBook = node.getPlace ().book; 
				if (!books.containsKey (prevBook)) continue; 
				labels.add (new Bible.Chapter (node.getPlace ())); 
				items.add (i); 
			} 
		} 
		
		public Bible.ReadNode getItem (int position) { 
			for (int i = 0; i < items.size (); i++) { 
				if (items.get (i) + i == position) return labels.get (i); 
				if (items.get (i) + i > position) return readNodes[position - i]; 
			} 
			return readNodes[position - items.size ()]; 
		} 
		@Override public int getItemViewType (int position) { 
			String type = getItem (position).getType (); 
			switch (type) { 
				case Bible.ReadNode.TYPE_VERSE: 
					return VIEW_TYPE_VERSE; 
				case Bible.ReadNode.TYPE_HEADER: 
					return VIEW_TYPE_HEADER; 
				case Bible.ReadNode.TYPE_CHAPTER: 
					return VIEW_TYPE_CHAPTER; 
				default: 
					return 0; 
			} 
		} 
		
		public void setReadNodes (Bible.ReadNode list []) { readNodes = list; setupItems (); } 
		public void setBooks (Bible.Book list []) { 
			for (Bible.Book book : list) 
				books.put (book.getSym (), book); 
			setupItems (); 
		} 
		
		public void setScale (float scale) { fontScale = scale; notifyDataSetChanged (); } 
		
		public void setFontScale (float s) { setScale (s); } 
		public float getFontScale () { return fontScale; } 
		
		public void setFoldParentheticalText (boolean whether_need_fold) { foldParentheticalText = whether_need_fold; } 
		
		public void setInteractionListener (ReadNodeInteractionListener l) { 
			listener = l; 
		} 
		
		public interface ReadNodeInteractionListener { 
			void onVerseViewClick (View v); 
			void onVerseViewLinkClick (View v, int linkIndex); 
			void onVerseViewLongClick (View v); 
		} 
	} 
	
	private static class DatabaseHelper extends SQLiteAssetHelper { 
		DatabaseHelper (Context ctx) { super (ctx, DB_NAME, null, DB_VER); } 
		DatabaseHelper (Context ctx, String filename) { super (ctx, filename, null, DB_VER); } 
	} 
	private static class BuilderDatabaseHelper extends SQLiteOpenHelper { 
		BuilderDatabaseHelper (Context ctx) { super (ctx, DB_NAME, null, DB_VER); } 
		BuilderDatabaseHelper (Context ctx, String name) { super (ctx, name, null, DB_VER); } 
		@Override public void onCreate (SQLiteDatabase db) { 
			BookTable.db_create (db); 
			VerseTable.db_create (db); 
		} 
		@Override public void onUpgrade (SQLiteDatabase db, int from, int to) { 
			Log.w (LOG_TAG, "Upgrading from version " + from + " to " + to); 
			BookTable.db_upgrade (db, from); 
			VerseTable.db_upgrade (db, from); 
		} 
	} 
	
	public void clear (boolean whether_recreate_with_fts3) { 
		// Clears ALL data from the DB, if writable: 
		if (mDb == null) 
			throw new IllegalStateException ("Must open () first!"); 
		BookTable.db_delete (mDb); 
		VerseTable.db_delete (mDb); 
		// Recreate the Tables: 
		if (whether_recreate_with_fts3) { 
			BookTable.db_create_fts3 (mDb); 
			VerseTable.db_create_fts3 (mDb); 
		} else { 
			BookTable.db_create (mDb); 
			VerseTable.db_create (mDb); 
		} 
	} 
	
	// BibleDbAdapter Methods: 
	public BibleDbAdapter (Context ctx) { mCtx = ctx; } 
	
	public BibleDbAdapter open (String db_name) { 
		if (isOpen ()) return this; 
		mDbHelper = new DatabaseHelper (mCtx, db_name); 
		mDb = mDbHelper.getWritableDatabase (); 
		TrackFTS3.ensureExists (mDb); 
		return this; 
	} 
	public BibleDbAdapter localOpen (String db_name) { 
		if (isOpen ()) return this; 
		mDbHelper = new BuilderDatabaseHelper (mCtx, db_name); 
		mDb = mDbHelper.getWritableDatabase (); 
		TrackFTS3.ensureExists (mDb); 
		return this; 
	} 
	public void close () { 
		if (!isOpen ()) return; 
		mDb.close (); 
		mDb = null; 
		mDbHelper.close (); 
		mDbHelper = null; 
	} 
	public boolean isOpen () { return mDb != null; } 
	public String getCurrentTimestamp () { 
		return new SimpleDateFormat (DB_DATE_FORMAT).format (new Date ()); 
	} 
	
	public boolean areTablesFTS3 () { 
		return books.isFTS3 () && verses.isFTS3 (); 
	} 
	public boolean convertTablesToFTS3 () { 
		boolean success = true; 
		if (!books.isFTS3 ()) success = books.convert2fts3 (); 
		if (!verses.isFTS3 ()) success &= verses.convert2fts3 (); 
		return success; 
	} 
	
	public void copyToDir (File dir) { 
		File from = mCtx.getDatabasePath (DB_NAME); 
		if (!from.exists ()) throw new Resources.NotFoundException ("Database file not found: " + DB_NAME); 
		File to = new File (dir, DB_NAME + "_copy.db"); 
		FileInputStream input = null; 
		FileOutputStream output = null; 
		try { 
			input = new FileInputStream (from);
			output = new FileOutputStream (to); 
			long sz = input.getChannel ().size (); 
			long done = output.getChannel ().transferFrom (input.getChannel (), 0, sz); 
			if (done != sz) throw new Error ("Transferred " + done + " out of " + sz + " bytes"); 
		} catch (IOException err) { 
			Log.e (LOG_TAG, "IO error while trying to copy DB to external storage!"); 
		} finally { 
			if (input != null) try { input.close (); } catch (IOException err) {} 
			if (output != null) try { output.close (); } catch (IOException err) {} 
		} 
	} 
	
	// DbContext Interface Implementation: 
	@Override public SQLiteDatabase getDB () { return mDb; } 
	@Override public Context getAndroidCtx () { return mCtx; } 
	@Override public String applyModifiers (String originalText) { 
		if (simplifyChinese) 
			return zhConverter.convert (originalText); 
		else return originalText; 
	} 
	@Override public String getSqliteSearchMatch (String compareValueTo) { 
		return ZHConverter.convert (compareValueTo, ZHConverter.TRADITIONAL); 
	} 
	@Override public void failIfNoDB () { if (mDb == null) throw new IllegalStateException ("Must open () first"); } 
} 
