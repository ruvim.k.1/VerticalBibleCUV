package net.wbord.verticalbible;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ChapterSelectFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ChapterSelectFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChapterSelectFragment extends Fragment {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_CHAPTER_COUNT = "chapter_count"; 
	
	public static final String KEY_CHAPTER_NUMBER = "chapter_number"; 

	// TODO: Rename and change types of parameters
	private int chapterCount = 0; 
	private int selectedChapter = -1; 

	private OnFragmentInteractionListener mListener;
	
	VerticalListView chapterList = null; 
	ChapterAdapter adapter = null; 

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param chapter_count Parameter 1. The number of chapters for the user to choose from. 
	 * @return A new instance of fragment ChapterSelectFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static ChapterSelectFragment newInstance (int chapter_count) {
		ChapterSelectFragment fragment = new ChapterSelectFragment (); 
		Bundle args = new Bundle (); 
		args.putInt (ARG_CHAPTER_COUNT, chapter_count); 
		fragment.setArguments (args); 
		return fragment; 
	}

	public ChapterSelectFragment () {
		// Required empty public constructor
	}

	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState); 
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container,
							  Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View main = inflater.inflate (R.layout.fragment_chapter_select, container, false); 
		chapterList = (VerticalListView) main.findViewById (R.id.chapter_select_list); 
		chapterList.setLayoutManager (new LinearLayoutManager (getActivity (), LinearLayoutManager.HORIZONTAL, true)); 
		chapterList.setAdapter (adapter = new ChapterAdapter (getActivity (), R.layout.chapter_list_item)); 
		adapter.setClickListener (new ChapterAdapter.OnClickListener () {
			@Override
			public void onChapterViewClicked (View v) {
				int position = chapterList.getChildAdapterPosition (v); 
				int chapter = 1 + position; 
				if (mListener != null) 
					mListener.onChapterSelect (chapter); 
			}
		}); 
		adapter.setChapterCount (chapterCount); 
		adapter.setSelectedChapter (selectedChapter); 
		return main; 
	}


	@Override
	public void onAttach (Activity activity) {
		super.onAttach (activity);
		
	}

	@Override
	public void onDetach () {
		super.onDetach ();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		void onChapterSelect (int chapterNumber);
	} 
	
	public void setInteractionListener (OnFragmentInteractionListener l) { 
		mListener = l; 
	} 
	public void setChapterCount (int count) { 
		chapterCount = count; 
		if (adapter == null) return; 
		adapter.setChapterCount (count); 
		adapter.notifyDataSetChanged (); 
	} 
	public void setSelectedChapter (int chapter) { 
		selectedChapter = chapter; 
		if (adapter == null) return; 
		adapter.setSelectedChapter (chapter); 
		adapter.notifyDataSetChanged (); 
	} 
	public int getSelectedChapter () { return adapter.getSelectedChapter (); } 
	
	
	public static class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.Holder> { 
		private int chapterCount = 0; 
		private final Context mCtx; 
		private final int mLayout; 
		private int selectedChapter = -1; 
		private OnClickListener listener = null; 
		public class Holder extends RecyclerView.ViewHolder { 
			final TextView vNumber; 
			final View vParent; 
			Holder (View parent) { 
				super (parent); 
				parent.setClickable (true); 
				parent.setOnClickListener (new View.OnClickListener () {
					@Override
					public void onClick (View v) {
						if (listener != null) 
							listener.onChapterViewClicked (v); 
					}
				}); 
				vParent = parent; 
				vNumber = (TextView) parent.findViewById (R.id.chapter_number); 
			} 
			public void bind (int chapterNumber) { 
				vNumber.setText (String.valueOf (chapterNumber)); 
				if (chapterNumber == selectedChapter) 
					vParent.setBackgroundResource (R.color.highlighted_text_material_dark); 
				else 
					vParent.setBackgroundColor (Color.TRANSPARENT); 
			} 
		} 
		public ChapterAdapter (Context ctx, int layout_id) { 
			mCtx = ctx; 
			mLayout = layout_id; 
		} 
		public Holder onCreateViewHolder (ViewGroup parent, int viewType) { 
			View vItem = LayoutInflater.from (parent.getContext ()).inflate (mLayout, parent, false); 
			return new Holder (vItem); 
		} 
		public void onBindViewHolder (Holder holder, int position) { 
			holder.bind (position + 1); 
		} 
		public int getItemCount () { return chapterCount; } 
		
		public void setClickListener (OnClickListener l) { listener = l; } 
		public void setChapterCount (int count) { chapterCount = count; } 
		public void setSelectedChapter (int sel) { selectedChapter = sel; } 
		public int getSelectedChapter () { return selectedChapter; } 
		
		public interface OnClickListener { 
			void onChapterViewClicked (View v); 
		} 
	} 
	
}
