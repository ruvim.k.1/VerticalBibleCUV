package net.wbord.verticalbible;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import java.util.Vector;

/**
 * Created by SpongeBob on 10.08.2015.
 */
public class ChooseBookFragment extends Fragment { 
	public static final int SECTION_JIU_YUE = 1; 
	public static final int SECTION_XIN_YUE = 2; 
	public static final int SECTION_ZUI_ZIN = 0; 
	
	public static final String KEY_SECTION_ID = "section"; 
	public static final String KEY_BOOK_SYMBOL = "book_symbol"; 
	
	public static final String STATE_SCROLL = "book_selection_scroll"; 
	public static final String STATE_SCALE = "scale_zoom"; 
	
	private int currentSection = SECTION_ZUI_ZIN; 
	
	private Bible.Book selected = null; 
	private Bible.Book bookNames [] = new Bible.Book [0]; 
	private ReadingHistory.Entry recent [] = new ReadingHistory.Entry [0]; 
	
	private BookSelectListener bookSelectListener = null; 
	
	private View mainLayout = null; 
	private RecyclerView vBooks = null; 
	private BibleDbAdapter.BookListAdapter listAdapter = null; 
	private Parcelable scrollState = null; 
	
	@Override 
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) { 
		mainLayout = inflater.inflate (R.layout.fragment_choose_book, container, false); 
		vBooks = (RecyclerView) mainLayout.findViewById (R.id.book_names); 
		listAdapter = new BibleDbAdapter.BookListAdapter (getActivity (), R.layout.book_list_item); 
		listAdapter.setBookSelectListener (new BibleDbAdapter.BookListAdapter.ListItemClicked () {
			@Override
			public void onBookClicked (View v) {
				int position = vBooks.getChildAdapterPosition (v); 
				Bible.Book book = listAdapter.getItem (position); 
				if (bookSelectListener != null) 
					bookSelectListener.onBookSelected (book); 
			}
		});
		Bundle args = getArguments (); 
		if (args != null) { 
			if (args.containsKey (KEY_SECTION_ID)) 
				setCurrentSection (args.getInt (KEY_SECTION_ID)); 
		} 
		if (savedInstanceState != null) { 
			scrollState = savedInstanceState.getParcelable (STATE_SCROLL); 
			vBooks.getLayoutManager ().onRestoreInstanceState (scrollState); 
			if (savedInstanceState.containsKey (STATE_SCALE)) 
				listAdapter.setFontScale (savedInstanceState.getFloat (STATE_SCALE)); 
		} 
		fillOutSection (); 
		return mainLayout; 
	} 
	
	@Override public void onSaveInstanceState (Bundle outState) { 
		outState.putParcelable (STATE_SCROLL, vBooks.getLayoutManager ().onSaveInstanceState ()); 
		if (listAdapter != null) 
			outState.putFloat (STATE_SCALE, listAdapter.getFontScale ()); 
	} 
	
	private void fillOutSection () {
		Vector<Bible.Book> books = new Vector<> (); 
		int i; 
		if (listAdapter == null) 
			return; 
		if (currentSection == SECTION_ZUI_ZIN) { 
			for (i = 0; i < recent.length && i < 24; i++) {
				String s = recent[i].started.toString (); 
				books.add (new Bible.Book (s, s, s, Bible.IS_NT)); 
			} 
			listAdapter.setBooks (booksVectorToArray (books)); 
			vBooks.setAdapter (listAdapter); 
			return; 
		} 
		for (i = 0; i < bookNames.length; i++) { 
			if (bookNames[i].getWhichOtNt ().equals (Bible.IS_OT) && currentSection == SECTION_XIN_YUE) continue; 
			if (bookNames[i].getWhichOtNt ().equals (Bible.IS_NT) && currentSection == SECTION_JIU_YUE) continue; 
			books.add (bookNames[i]); 
		} 
		listAdapter.setBooks (booksVectorToArray (books)); 
		vBooks.setAdapter (listAdapter); 
		if (scrollState != null) 
			vBooks.getLayoutManager ().onRestoreInstanceState (scrollState); 
	} 
	public Bible.Book [] booksVectorToArray (Vector<Bible.Book> books) {
		Bible.Book arr [] = new Bible.Book [books.size ()]; 
		int i; 
		for (i = 0; i < books.size (); i++) arr[i] = books.get (i); 
		return arr; 
	} 
	
	public void setCurrentSection (int set_to) { currentSection = set_to; fillOutSection (); } 
	public int getCurrentSection () { return currentSection; } 
	
	public void setRecent (ReadingHistory.Entry list []) { recent = list; fillOutSection (); } 
	public void setBookNames (Bible.Book list []) { 
		bookNames = list; 
		if (selected != null) 
			setSelected (selected); 
		fillOutSection (); 
	} 
	public void setBookSelectListener (BookSelectListener listener) { 
		bookSelectListener = listener; 
	} 
	
	public void setSelected (Bible.Book book) { 
		if (selected != null) selected.выделить = false; 
		Bible.Book found = null; 
		if (book == null) { 
			if (listAdapter != null) 
				listAdapter.notifyDataSetChanged (); 
			selected = null; 
			return; 
		} 
		for (int i = 0; i < bookNames.length; i++) 
			if (bookNames[i].equals (book)) { 
				found = bookNames[i]; 
				break; 
			} 
		if (found == null) { 
			selected = book; 
			return; 
		} 
		found.выделить = true; 
		selected = found; 
		if (listAdapter != null) 
			listAdapter.notifyDataSetChanged (); 
	} 
	public Bible.Book getSelected () { return selected; } 
	
	public interface BookSelectListener { 
		void onBookSelected (Bible.Book book); 
	} 
} 
