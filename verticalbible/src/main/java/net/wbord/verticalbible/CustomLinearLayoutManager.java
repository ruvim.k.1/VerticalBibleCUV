package net.wbord.verticalbible;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import java.lang.reflect.Field;

/**
 * Created by SpongeBob on 13.08.2015.
 */
public class CustomLinearLayoutManager extends LinearLayoutManager { 
	private static boolean canMakeInsetsDirty = true; 
	private static Field insetsDirtyField = null; 
	
	private static final int CHILD_WIDTH = 0; 
	private static final int CHILD_HEIGHT = 0; 
	private static final int DEFAULT_CHILD_SIZE = 100; 
	
	private final int childDimensions [] = new int [2]; 
	private RecyclerView recyclerView = null; 
	
	private int childSize = DEFAULT_CHILD_SIZE; 
	private boolean hasChildSize = false; 
	private int overScrollMode = ViewCompat.OVER_SCROLL_ALWAYS; 
	private final Rect tmpRectangle = new Rect (); 
	
	@SuppressWarnings ("UnusedDeclaration") 
	public CustomLinearLayoutManager (Context context, int orientation, boolean reverseLayout) { 
		super (context, orientation, reverseLayout); 
	} 
	@SuppressWarnings ("UnusedDeclaration") 
	public CustomLinearLayoutManager (Context context) { 
		super (context); 
	} 
	@SuppressWarnings ("UnusedDeclaration") 
	public CustomLinearLayoutManager (Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) { 
		super (context, attrs, defStyleAttr, defStyleRes); 
	} 
	
	public static int makeUnspecifiedSpec () { 
		return View.MeasureSpec.makeMeasureSpec (0, View.MeasureSpec.UNSPECIFIED); 
	} 
	
	@Override public void onMeasure (RecyclerView.Recycler recycler, RecyclerView.State state, 
									 int widthMeasureSpec, int heightMeasureSpec) { 
		final int wMode = View.MeasureSpec.getMode (widthMeasureSpec); 
		final int wSize = View.MeasureSpec.getSize (widthMeasureSpec); 
		final int hMode = View.MeasureSpec.getMode (heightMeasureSpec); 
		final int hSize = View.MeasureSpec.getSize (heightMeasureSpec); 
		
		if (wMode == View.MeasureSpec.EXACTLY && hMode == View.MeasureSpec.EXACTLY) { 
			super.onMeasure (recycler, state, widthMeasureSpec, heightMeasureSpec); 
			return; 
		}
		
		final int unspecified = makeUnspecifiedSpec (); 
		
		final boolean isVertical = getOrientation () == VERTICAL; 
		
		
		
		int width = 0; 
		int height = 0; 
		
		
	} 
	
	public void initChildDimensions () { 
		
	} 
} 
