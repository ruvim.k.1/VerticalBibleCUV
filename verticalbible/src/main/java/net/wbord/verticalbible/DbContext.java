package net.wbord.verticalbible;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by SpongeBob on 10.08.2015.
 */
public interface DbContext { 
	SQLiteDatabase getDB (); 
	Context getAndroidCtx (); 
	String applyModifiers (String originalText); 
	String getSqliteSearchMatch (String compareValueTo); 
	void failIfNoDB (); 
} 
