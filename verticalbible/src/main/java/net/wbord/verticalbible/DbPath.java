package net.wbord.verticalbible;

/**
 * Created by SpongeBob on 11.08.2015.
 */
public interface DbPath { 
	String getDbName (); 
	String getDownloadURL (); 
} 