package net.wbord.verticalbible;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DisplayFoldedTextFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DisplayFoldedTextFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DisplayFoldedTextFragment extends Fragment {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_TEXT = "folded_text"; 
	
	// TODO: Rename and change types of parameters
	private String mText = ""; 
	
	private VerticalTextView vNoteText = null; 
	
	private OnFragmentInteractionListener mListener;

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param text Parameter 1.
	 * @return A new instance of fragment DisplayFoldedTextFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static DisplayFoldedTextFragment newInstance (String text) {
		DisplayFoldedTextFragment fragment = new DisplayFoldedTextFragment ();
		Bundle args = new Bundle (); 
		args.putString (ARG_TEXT, text); 
		fragment.setArguments (args);
		return fragment;
	}

	public DisplayFoldedTextFragment () {
		// Required empty public constructor
	}

	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		if (getArguments () != null) {
			setText (getArguments ().getString (ARG_TEXT)); 
		} 
	}

	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container,
							  Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View mainView = inflater.inflate (R.layout.fragment_display_folded_text, container, false); 
		vNoteText = (VerticalTextView) mainView.findViewById (R.id.folded_text_view); 
		vNoteText.setText (mText); 
		return mainView;
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed (Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction (uri);
		}
	}
	
	@Override
	public void onAttach (Activity activity) {
		super.onAttach (activity); 
	}

	@Override
	public void onDetach () {
		super.onDetach ();
		mListener = null;
	} 
	
	public void setText (String txt) { 
		mText = txt; 
		if (vNoteText != null) 
			vNoteText.setText (txt); 
		if (getArguments () != null) 
			getArguments ().putString (ARG_TEXT, txt); 
	} 

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p/>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction (Uri uri); 
	}

}
