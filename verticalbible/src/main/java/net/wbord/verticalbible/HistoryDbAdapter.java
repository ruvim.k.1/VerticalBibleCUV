package net.wbord.verticalbible;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by SpongeBob on 10.08.2015.
 */
public class HistoryDbAdapter implements DbContext { 
	// Database name and version: 
	public static final String DB_NAME = "read_history"; 
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"; 
	public static final int DB_VER = 1; 
	// Access: 
	public HistoryTable historyTable = new HistoryTable (this); 
	// Log Tag: 
	private static final String LOG_TAG = "HistoryDB"; 
	// Private Variables: 
	private DatabaseHelper mDbHelper = null; 
	private SQLiteDatabase mDb = null; 
	private Context mCtx; 
	// Classes: 
	public static abstract class OnLoadFinished<TYPE> implements Runnable { 
		public TYPE result []; 
	} 
	public static class HistoryTable { 
		public static final String NAME = "history_table"; 
		public static class Keys { 
			public static final String ID = "_id"; 
			public static final String TIME = "timestamp"; 
			public static class StartedReading { 
				public static final String BOOK = "start_book"; 
				public static final String CHAPTER = "start_chapter"; 
				public static final String VERSE = "start_verse"; 
			} 
			public static class StoppedReading { 
				public static final String BOOK = "stop_book"; 
				public static final String CHAPTER = "stop_chapter"; 
				public static final String VERSE = "stop_verse"; 
			} 
		} 
		// Context: 
		private DbContext dbContext = null; 
		// Constructor: 
		public HistoryTable (DbContext ctx) { dbContext = ctx; } 
		// Static Methods: 
		public static void db_create (SQLiteDatabase db) { 
			db.execSQL ("create table " + NAME + " (" + 
								Keys.ID + " integer primary key autoincrement, " + 
								Keys.TIME + " text not null, " + 
								Keys.StartedReading.BOOK + " text not null, " + 
								Keys.StartedReading.CHAPTER + " integer not null, " + 
								Keys.StartedReading.VERSE + " integer, " + 
								Keys.StoppedReading.BOOK + " text, " + 
								Keys.StoppedReading.CHAPTER + " integer, " + 
								Keys.StoppedReading.VERSE + " integer" + 
			");"); 
		} 
		public static void db_delete (SQLiteDatabase db) { 
			db.execSQL ("DROP TABLE IF EXISTS " + NAME); 
		} 
		public static void db_upgrade (SQLiteDatabase db, int old_version) { 
			db_delete (db); 
			db_create (db); 
		} 
		// Public Access Methods: 
		public ReadingHistory.Entry insert (ReadingHistory.Entry entry) {
			dbContext.failIfNoDB (); 
			ContentValues vals = new ContentValues (); 
			String time = entry.time; 
			if (time.isEmpty ()) time = getCurrentTimestamp (); 
			vals.put (Keys.TIME, time); 
			vals.put (Keys.StartedReading.BOOK, entry.started.book); 
			vals.put (Keys.StartedReading.CHAPTER, entry.started.getChapter ()); 
			vals.put (Keys.StartedReading.VERSE, entry.started.getVerse ()); 
			vals.put (Keys.StoppedReading.BOOK, entry.stopped.book); 
			vals.put (Keys.StoppedReading.CHAPTER, entry.stopped.getChapter ()); 
			vals.put (Keys.StoppedReading.VERSE, entry.stopped.getVerse ()); 
			entry.id = (int) dbContext.getDB ().insert (NAME, null, vals); 
			return entry; 
		} 
		public boolean update (int id, 
											Bible.VersePointer started, 
											Bible.VersePointer stopped) { 
			dbContext.failIfNoDB (); 
			ContentValues vals = new ContentValues (); 
			vals.put (Keys.TIME, getCurrentTimestamp ()); 
			if (started != null) { 
				vals.put (Keys.StartedReading.BOOK, started.book); 
				vals.put (Keys.StartedReading.CHAPTER, started.getChapter ()); 
				vals.put (Keys.StartedReading.VERSE, started.getVerse ()); 
			} 
			if (stopped != null) { 
				vals.put (Keys.StoppedReading.BOOK, stopped.book); 
				vals.put (Keys.StoppedReading.CHAPTER, stopped.getChapter ()); 
				vals.put (Keys.StoppedReading.VERSE, stopped.getVerse ()); 
			} 
			return dbContext.getDB ().update (NAME, vals, Keys.ID + " = " + id, null) > 0; 
		} 
		public Cursor fetch () { 
			SQLiteDatabase db = dbContext.getDB (); 
			if (db == null) { 
				return null; 
			} 
			return db.query (NAME, new String [] { 
				Keys.ID, Keys.TIME, 
				Keys.StartedReading.BOOK, Keys.StartedReading.CHAPTER, Keys.StartedReading.VERSE, 
				Keys.StoppedReading.BOOK, Keys.StoppedReading.CHAPTER, Keys.StoppedReading.VERSE 
			}, null, null, null, null, Keys.TIME + " DESC"); 
		} 
		public void fetchAsync (final OnLoadFinished<ReadingHistory.Entry> callback) { 
			dbContext.failIfNoDB (); 
			Thread thread = new Thread (new Runnable () {
				@Override
				public void run () {
					Cursor c = fetch (); 
					int i = 0; 
					callback.result = new ReadingHistory.Entry [c.getCount ()]; 
					c.moveToFirst (); 
					while (!c.isAfterLast ()) { 
						ReadingHistory.Entry entry = callback.result[i] = new ReadingHistory.Entry (); 
						entry.id = c.getInt (0); 
						entry.time = c.getString (1); 
						entry.started.set (c.getString (c.getColumnIndex (Keys.StartedReading.BOOK)), 
												  c.getInt (c.getColumnIndex (Keys.StartedReading.CHAPTER)), 
												  c.getInt (c.getColumnIndex (Keys.StartedReading.VERSE))); 
						entry.stopped.set (c.getString (c.getColumnIndex (Keys.StoppedReading.BOOK)), 
												  c.getInt (c.getColumnIndex (Keys.StoppedReading.CHAPTER)), 
												  c.getInt (c.getColumnIndex (Keys.StoppedReading.VERSE))); 
						// Continue: 
						c.moveToNext (); 
						i++; 
					} 
					c.close (); 
					((Activity) dbContext.getAndroidCtx ()).runOnUiThread (callback); 
				}
			}); 
			thread.start (); 
		} 
		public void cleanupAsync (int delete_if_older_than) { 
			dbContext.failIfNoDB (); 
			long span = delete_if_older_than * 24 * 60 * 60 * 1000; 
			Date keepSince = new Date (); 
			keepSince.setTime (keepSince.getTime () - span); 
			final String since = new SimpleDateFormat (DB_DATE_FORMAT).format (keepSince); 
			Thread thread = new Thread (new Runnable () {
				@Override
				public void run () {
					dbContext.getDB ().delete (NAME, Keys.TIME + " < " + since, null); 
				}
			}); 
			thread.start (); 
		} 
	} 
	private static class DatabaseHelper extends SQLiteOpenHelper { 
		DatabaseHelper (Context ctx) { super (ctx, DB_NAME, null, DB_VER); } 
		@Override 
		public void onCreate (SQLiteDatabase db) { 
			HistoryTable.db_create (db); 
		} 
		public void onUpgrade (SQLiteDatabase db, int from, int to) {
			Log.w (LOG_TAG, "Upgrading DB from " + from + " to " + to); 
			HistoryTable.db_upgrade (db, from); 
		} 
	} 
	
	// DB Adapter Methods: 
	public HistoryDbAdapter (Context ctx) { 
		mCtx = ctx; 
	} 
	public HistoryDbAdapter open () { 
		if (isOpen ()) return this; 
		mDbHelper = new DatabaseHelper (mCtx); 
		mDb = mDbHelper.getWritableDatabase (); 
		return this; 
	} 
	public void close () { 
		if (!isOpen ()) return; 
		mDb.close (); 
		mDb = null; 
		mDbHelper.close (); 
		mDbHelper = null; 
	} 
	public boolean isOpen () { return mDb != null; } 
	public static String getCurrentTimestamp () { 
		return new SimpleDateFormat (DB_DATE_FORMAT).format (new Date ()); 
	} 
	
	// Methods to get DB and context: 
	@Override 
	public SQLiteDatabase getDB () { return mDb; } 
	@Override 
	public Context getAndroidCtx () { return mCtx; } 
	@Override public String applyModifiers (String originalText) { return originalText; } 
	@Override public String getSqliteSearchMatch (String compareValueTo) { return compareValueTo; } 
	@Override 
	public void failIfNoDB () { if (mDb == null) throw new IllegalStateException ("Must call open () first"); } 
} 
