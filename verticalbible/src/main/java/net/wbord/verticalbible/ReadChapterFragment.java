package net.wbord.verticalbible;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

/**
 * A placeholder fragment containing a simple view.
 */
public class ReadChapterFragment extends Fragment 
		implements BibleDbAdapter.ReadNodeListAdapter.ReadNodeInteractionListener { 
	public static final int STATE_EMPTY = 0; 
	public static final int STATE_LOADING = 1; 
	public static final int STATE_READY = 2; 
	
	public static final String KEY_WHETHER_READSPACE_MODE = "whether_readspace_mode"; 
	public static final String KEY_CONTROL_PANEL_LAYOUT_ID = "control_panel_layout_id";  
	public static final String KEY_VERSE_NUMBER = "verse_number"; 
	
	public static final String STATE_READSPACE_MODE = "readspace_mode"; 
	public static final String STATE_FOLDED_VISIBLE = "folded_visible"; 
	
	public static final String STATE_BOOK = "book"; 
	public static final String STATE_LOAD_DESC = "load_page_descriptor"; 
	public static final String STATE_SEL_VERSE = "selected_verse"; 
	public static final String STATE_SEL_LINK = "selected_link"; 
	
	public static final String STATE_SCALE = "scale_zoom"; 
	public static final String STATE_SCROLL = "chapter_scroll"; 
//	public static final String STATE_SCROLL_TYPE = "scroll_type"; 
//	public static final String STATE_SCROLL_POSITION = "scroll_position"; 
//	public static final String STATE_SCROLL_OFFSET = "scroll_offset"; 
	
	private int state = STATE_EMPTY; 
	
	private boolean inReadspaceMode = false; 
	
	private boolean foldedTextViewVisible = false; 
	
	private Bible.LoadPageDescriptor loadDesc = null; 
	
	private Bible.Book book = null; 
	private Bible.Book bookList [] = null; 
	private Bible.ReadNode read [] = null; 
	
	private Bible.ReadNode selectedVerse = null; 
	private int selectedLinkIndex = 0; 
	
	private OnVerseInteractionListener listener = null; 
	private VerticalListView vReadList = null; 
	private BibleDbAdapter.ReadNodeListAdapter listAdapter = null; 
	private Parcelable scrollState = null; 
	
	private ProgressBar vLoading = null; 
	private TextView vEmpty = null; 
	
	private ControlPanelViewHolder controlPanelViewHolder = null; 
	private ControlPanelViewAdapter controlPanel = null; 
	private DisplayFoldedTextFragment fDisplayNote = null; 
	
	private Bible.LoadPageDescriptor needLoad = null; 
	private OnReadchapterViewCreateListener readchapterViewCreateListener = null; 
	
	public ReadChapterFragment () {
	}
	
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container,
							  Bundle savedInstanceState) {
		View main = inflater.inflate (R.layout.fragment_read_chapter, container, false); 
		Bundle args = getArguments (); 
		if (args != null) readArguments (args); 
		fDisplayNote = DisplayFoldedTextFragment.newInstance (""); 
		vEmpty = (TextView) main.findViewById (R.id.emptyText); 
		vLoading = (ProgressBar) main.findViewById (R.id.loadingProgressBar); 
		vReadList = (VerticalListView) main.findViewById (R.id.verse_read_list); 
		vReadList.setAdapter (listAdapter = 
			new BibleDbAdapter.ReadNodeListAdapter (getActivity ())); 
		listAdapter.setInteractionListener (this); 
		if (read != null) listAdapter.setReadNodes (read); 
		if (savedInstanceState != null) 
			loadState (savedInstanceState); 
		if (readchapterViewCreateListener != null) 
			readchapterViewCreateListener.onReadchapterViewCreate (main); 
		if (controlPanel != null) { 
			FrameLayout frame = (FrameLayout) main.findViewById (R.id.readspace_control_panel_container); 
			controlPanelViewHolder = controlPanel.onCreateViewHolder (frame, 0); 
			controlPanelViewHolder.addViewToLayout (frame); 
			if (controlPanelViewHolder != null) 
				controlPanel.onBindViewHolder (controlPanelViewHolder, 0); 
		} else controlPanelViewHolder = null; 
		setState (state); // Set state again, in case setState () was called earlier ... 
		return main; 
	} 
	@Override public void onSaveInstanceState (Bundle outState) { 
		super.onSaveInstanceState (outState); 
		saveState (outState); 
	} 
	
	public void updateControlPanel () { 
		if (controlPanelViewHolder != null) 
			controlPanel.onBindViewHolder (controlPanelViewHolder, 0); 
	} 
	
	private void saveState (Bundle outState) { 
		outState.putBoolean (STATE_READSPACE_MODE, inReadspaceMode); 
		if (loadDesc != null) 
			outState.putBundle (STATE_LOAD_DESC, loadDesc.saveState ()); 
		outState.putBoolean (STATE_FOLDED_VISIBLE, foldedTextViewVisible); 
		if (book != null) 
			outState.putBundle (STATE_BOOK, book.saveState ()); 
		if (selectedVerse != null) 
			outState.putBundle (STATE_SEL_VERSE, selectedVerse.getPlace ().saveState ()); 
		outState.putInt (STATE_SEL_LINK, selectedLinkIndex); 
		// Scroll state: 
		outState.putParcelable (STATE_SCROLL, vReadList.getLayoutManager ().onSaveInstanceState ()); 
		// Zoom Level: 
		outState.putFloat (STATE_SCALE, listAdapter.getFontScale ()); 
	} 
	private void loadState (Bundle inState) { 
		if (book == null) book = new Bible.Book (); 
		inReadspaceMode = inState.getBoolean (STATE_READSPACE_MODE); 
		foldedTextViewVisible = inState.getBoolean (STATE_FOLDED_VISIBLE); 
		if (book != null && inState.containsKey (STATE_BOOK)) 
			book.loadState (inState.getBundle (STATE_BOOK)); 
		if (inState.containsKey (STATE_LOAD_DESC)) {
			if (loadDesc == null) loadDesc = new Bible.LoadPageDescriptor (); 
			loadDesc.loadState (inState.getBundle (STATE_LOAD_DESC)); 
			if (listener != null) 
				listener.requestChapterLoad (loadDesc); 
			else needLoad = loadDesc; 
		} 
		if (inState.containsKey (STATE_SEL_VERSE)) { 
			if (selectedVerse == null) 
				selectedVerse = new Bible.Verse (); 
			selectedVerse.getPlace ().loadState (inState.getBundle (STATE_SEL_VERSE)); 
		} else selectedVerse = null; 
		selectedLinkIndex = inState.getInt (STATE_SEL_LINK); 
		// Scroll state: 
		scrollState = inState.getParcelable (STATE_SCROLL); 
		if (scrollState != null) 
			vReadList.getLayoutManager ().onRestoreInstanceState (scrollState); 
		// Zoom Level: 
		listAdapter.setFontScale (inState.getFloat (STATE_SCALE)); 
	} 
	
	private void makeFoldedTextVisible (boolean visible) { 
		final String FRAGMENT_NAME = "displayNote"; 
		if (visible == foldedTextViewVisible) 
			return; 
		if (visible) { 
			getChildFragmentManager ().beginTransaction () 
					.setCustomAnimations (R.anim.left_slide_in, R.anim.left_slide_out) 
					.replace (R.id.display_note_fragment, fDisplayNote)
					.addToBackStack (FRAGMENT_NAME) 
					.commit (); 
//			fDisplayNote.startAnimation (slideIn); 
//			fDisplayNote.setVisibility (View.VISIBLE); 
		} else { 
//			Animation slideOut = AnimationUtils.makeOutAnimation (getActivity (), false); 
//			slideOut.setAnimationListener (new Animation.AnimationListener () {
//				@Override
//				public void onAnimationStart (Animation animation) {
//					
//				} 
//				@Override
//				public void onAnimationEnd (Animation animation) {
//					vFoldedText.setVisibility (View.GONE); 
//				} 
//				@Override
//				public void onAnimationRepeat (Animation animation) {
//					
//				}
//			}); 
//			vFoldedText.startAnimation (slideOut); 
			getChildFragmentManager () 
					.popBackStack (FRAGMENT_NAME, FragmentManager.POP_BACK_STACK_INCLUSIVE); 
		} 
		foldedTextViewVisible = visible; 
	} 
	
	public void readArguments (Bundle args) { 
		if (args == null) return; 
		inReadspaceMode = args.getBoolean (KEY_WHETHER_READSPACE_MODE); 
	} 
	
	@Override public void onVerseViewClick (View v) { 
		int position = vReadList.getChildAdapterPosition (v); 
		boolean overrideDefault = false; 
		if (listener != null)
			overrideDefault = listener.onVerseClick (listAdapter.getItem (position)); 
		if (overrideDefault) return; 
		selectedVerse = listAdapter.getItem (position); 
		selectedLinkIndex = -1; 
		makeFoldedTextVisible (false); 
	} 
	@Override public void onVerseViewLinkClick (View v, int linkIndex) { 
		int position = vReadList.getChildAdapterPosition (v); 
		boolean overrideDefault = false; 
		if (listener != null)
			overrideDefault = listener.onVerseLinkClick (listAdapter.getItem (position), linkIndex); 
		if (overrideDefault) return; 
		selectedVerse = listAdapter.getItem (position); 
		selectedLinkIndex = linkIndex; 
		fDisplayNote.setText (selectedVerse.getFoldedText (linkIndex)); 
		//vFoldedText.setText (selectedVerse.getFoldedText (linkIndex)); 
		makeFoldedTextVisible (true); 
	} 
	@Override public void onVerseViewLongClick (View v) { 
		int position = vReadList.getChildAdapterPosition (v); 
		if (listener != null) 
			listener.onVerseLongClick (listAdapter.getItem (position)); 
	} 
	
	public void setLoadDesc (Bible.LoadPageDescriptor desc) { 
		loadDesc = desc; 
		int verse = loadDesc.getPlace ().getVerse (); 
		if (verse > 1 && scrollState == null) 
			vReadList.getLayoutManager () 
			.scrollToPosition (getVersePosition (verse)); 
	} 
	public int getVersePosition (int verseNumber) { 
		int i; 
		for (i = 0; i < read.length; i++) 
			if (read[i].getPlace ().getVerse () == verseNumber) 
				return i; 
		return 0; 
	} 
	
	public void setState (int s) { 
		// Turn off previous: 
		if (state == STATE_EMPTY && vEmpty != null) 
			vEmpty.setVisibility (View.GONE); 
		else if (state == STATE_LOADING && vLoading != null) 
			vLoading.setVisibility (View.GONE); 
		// Change state: 
		state = s; 
		// Turn on current: 
		if (state == STATE_EMPTY && vEmpty != null) 
			vEmpty.setVisibility (View.VISIBLE); 
		else if (state == STATE_LOADING && vLoading != null) 
			vLoading.setVisibility (View.VISIBLE); 
	} 
	public void setStateEmpty () { setState (STATE_EMPTY); } 
	public void setStateLoading () { setState (STATE_LOADING); } 
	public void setStateReady () { setState (STATE_READY); } 
	
	public void setBook (Bible.Book the_book) { book = the_book; } 
	public void setReadList (Bible.ReadNode the_list []) { 
		read = the_list; 
		if (the_list.length > 0) 
			setStateReady (); 
		else 
			setStateEmpty (); 
		listAdapter.setReadNodes (the_list); 
		listAdapter.notifyDataSetChanged (); 
		if (scrollState != null) 
			vReadList.getLayoutManager ().onRestoreInstanceState (scrollState); 
	} 
	public void setBookList (Bible.Book the_list []) { 
		bookList = the_list; 
		listAdapter.setBooks (the_list); 
		listAdapter.notifyDataSetChanged (); 
		if (scrollState != null) 
			vReadList.getLayoutManager ().onRestoreInstanceState (scrollState); 
	} 
	public void resetReadPage () { 
		vReadList.setAdapter (vReadList.getAdapter ()); 
	} 
	public boolean isInReadspaceMode () { return inReadspaceMode; } 
	
	public Bible.Book getBook (Bible.VersePointer p) { 
		if (bookList == null) 
			return null; 
		for (Bible.Book book : bookList) 
			if (book.getSym ().equals (p.book)) 
				return book; 
		return null; 
	} 
	
	public boolean isFoldedTextViewVisible () { return foldedTextViewVisible; } 
	
	public void setOnVerseInteractionListener (OnVerseInteractionListener l) { 
		listener = l; 
		if (needLoad != null) { 
			listener.requestChapterLoad (needLoad); 
			needLoad = null; 
		} 
	} 
	
	public void setOnReadchapterViewCreateListener (OnReadchapterViewCreateListener listener) { 
		readchapterViewCreateListener = listener; 
	} 
	public void setControlPanelAdapter (ControlPanelViewAdapter adapter) { 
		controlPanel = adapter; 
	} 
	
	public static class ControlPanelViewHolder { 
		View mMainView = null; 
		public ControlPanelViewHolder (View parent) { mMainView = parent; } 
		public final void addViewToLayout (FrameLayout layout) { 
			layout.addView (mMainView); 
		} 
	} 
	public static abstract class ControlPanelViewAdapter<HOLDER extends ControlPanelViewHolder> { 
		public abstract HOLDER onCreateViewHolder (ViewGroup parent, int viewType); 
		public abstract void onBindViewHolder (HOLDER holder, int position); 
	} 
	
	public interface OnVerseInteractionListener { 
		boolean onVerseClick (Bible.ReadNode node); 
		boolean onVerseLinkClick (Bible.ReadNode node, int linkIndex); 
		void onVerseLongClick (Bible.ReadNode node); 
		void requestChapterLoad (Bible.LoadPageDescriptor load); 
	} 
	public interface OnReadchapterViewCreateListener { 
		void onReadchapterViewCreate (View v); 
	} 
}
