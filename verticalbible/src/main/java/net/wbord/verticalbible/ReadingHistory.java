package net.wbord.verticalbible;

/**
 * Created by SpongeBob on 10.08.2015.
 */
public class ReadingHistory { 
	
	public static class Entry { 
		public int id = -1; 
		public String time = ""; 
		public Bible.VersePointer started = new Bible.VersePointer (); 
		public Bible.VersePointer stopped = new Bible.VersePointer (); 
	} 
	
}
