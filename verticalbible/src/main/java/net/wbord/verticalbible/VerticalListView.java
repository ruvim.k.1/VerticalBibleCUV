package net.wbord.verticalbible;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;


/**
 * Created by SpongeBob on 10.08.2015.
 */
public class VerticalListView extends RecyclerView { 
	public interface ScaleableAdapter { 
		void setScale (float scale); 
	} 
	ScaleGestureDetector scaleDetector = null; 
	float fontScale = 1; 
	public VerticalListView (Context ctx, AttributeSet attrs) { 
		super (ctx, attrs); 
		setLayoutManager (new LinearLayoutManager (ctx, LinearLayoutManager.HORIZONTAL, true)); 
		scaleDetector = 
				new ScaleGestureDetector (getContext (), 
						 new ScaleGestureDetector.OnScaleGestureListener () { 
							 @Override 
							 public boolean onScale (ScaleGestureDetector detector) { 
								 float upScale = detector.getScaleFactor (); 
								 fontScale *= upScale; 
								 if (fontScale < 1) fontScale = 1; 
								 RecyclerView.Adapter adapter = getAdapter (); 
								 if (adapter instanceof ScaleableAdapter) 
									 ((ScaleableAdapter) adapter).setScale (fontScale); 
								 return true; 
							 } 
							 @Override 
							 public boolean onScaleBegin (ScaleGestureDetector detector) { 
								 return true; 
							 } 
							 @Override 
							 public void onScaleEnd (ScaleGestureDetector detector) { 
							 } 
						 }); 
	} 
	@Override public boolean onTouchEvent (@NonNull MotionEvent event) { 
		boolean up = super.onTouchEvent (event); 
		boolean det = scaleDetector.onTouchEvent (event); 
		return up || det; 
	} 
} 
