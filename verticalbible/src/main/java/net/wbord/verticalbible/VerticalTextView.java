package net.wbord.verticalbible;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.TextView;

import java.util.Vector;

/**
 * Created by SpongeBob on 10.08.2015.
 */
public class VerticalTextView extends View { 
	private String text = ""; 
	private String shortText = ""; 
	// Operation Variables: 
	Paint mainPaint = new Paint (); 
	Paint highlight = new Paint (); 
	Rect lMeasBounds = new Rect (); 
	Rect measBounds = new Rect (); 
	Rect charBounds = new Rect (); 
	Rect bounds = new Rect (); 
	int widestCharacterWidth = 0; 
	Vector<Highlight> highlights = new Vector<> (); 
	Vector<Pair<Integer,Integer>> links = new Vector<> (); 
	Vector<CharacterPosition> layoutBuffer = new Vector<> (); 
	// Style Attribute Parameters: 
	int spacingBetweenLines = 0; 
	int paddingTop = 0; 
	int paddingLeft = 0; 
	int paddingRight = 0; 
	int paddingBottom = 0; 
	int indentFirstLine = 0; 
	int indentAllLines = 0; 
	int textColor = Color.BLACK; 
	int textColorLink = Color.BLACK; 
	int highlightColor = Color.TRANSPARENT; 
	int highlightColorLink = Color.YELLOW;
	int shadowColor = Color.TRANSPARENT; 
	int textSize = 0; 
	float shadowDx = 0; 
	float shadowDy = 0; 
	float shadowRadius = 0; 
	float characterGridAlignLeft = 0; 
	float characterGridAlignTop = .5f; 
	float fontScale = 1; 
	private void loadAttributeArray (TypedArray style, Context ctx, AttributeSet attrs) {
		int count = style.getIndexCount (); 
		for (int i = 0; i < count; i++) {
			int attr = style.getIndex (i);
			// Android Attributes: 
			if (attr == R.styleable.VerticalTextView_android_textSize) 
				textSize = style.getDimensionPixelSize (attr, 14); 
			else if (attr == R.styleable.VerticalTextView_android_paddingLeft) 
				paddingLeft = style.getDimensionPixelOffset (attr, 0); 
			else if (attr == R.styleable.VerticalTextView_android_paddingTop) 
				paddingTop = style.getDimensionPixelOffset (attr, 0); 
			else if (attr == R.styleable.VerticalTextView_android_paddingRight) 
				paddingRight = style.getDimensionPixelOffset (attr, 0); 
			else if (attr == R.styleable.VerticalTextView_android_paddingBottom) 
				paddingBottom = style.getDimensionPixelOffset (attr, 0); 
			else if (attr == R.styleable.VerticalTextView_android_shadowRadius) 
				shadowRadius = style.getFloat (attr, 0f); 
			else if (attr == R.styleable.VerticalTextView_android_shadowDx) 
				shadowDx = style.getFloat (attr, 0f); 
			else if (attr == R.styleable.VerticalTextView_android_shadowDy) 
				shadowDy = style.getFloat (attr, 0f); 
			else if (attr == R.styleable.VerticalTextView_android_shadowColor) 
				shadowColor = style.getColor (attr, Color.TRANSPARENT); 
			else if (attr == R.styleable.VerticalTextView_android_textColorLink) 
				textColorLink = style.getColor (attr, Color.BLUE); 
			else if (attr == R.styleable.VerticalTextView_android_textColor) 
				textColor = style.getColor (attr, Color.BLACK); 
			else if (attr == R.styleable.VerticalTextView_android_text) 
				text = style.getString (attr); 
				// Our App Attributes: 
			else if (attr == R.styleable.VerticalTextView_characterGridAlignLeft) 
				characterGridAlignLeft = 
						style.getFloat (attr, characterGridAlignLeft); 
			else if (attr == R.styleable.VerticalTextView_characterGridAlignTop) 
				characterGridAlignTop = 
						style.getFloat (attr, characterGridAlignTop); 
			else if (attr == R.styleable.VerticalTextView_spacingBetweenLines) 
				spacingBetweenLines = style.getDimensionPixelOffset (attr, 0); 
			else if (attr == R.styleable.VerticalTextView_indentAllLines) 
				indentAllLines = style.getDimensionPixelOffset (attr, 0); 
			else if (attr == R.styleable.VerticalTextView_indentFirstLine) 
				indentFirstLine = style.getDimensionPixelOffset (attr, 0); 
			else if (attr == R.styleable.VerticalTextView_highlightColorLink) 
				highlightColorLink = style.getColor (attr, highlightColorLink); 
			else if (attr == R.styleable.VerticalTextView_highlightColor) 
				highlightColor = style.getColor (attr, highlightColor); 
			else if (attr == R.styleable.VerticalTextView_shortText) 
				shortText = style.getString (attr); 
			else if (attr == R.styleable.VerticalTextView_style_verticalTextView) { 
				int resID = style.getResourceId (attr, 0); 
				if (resID <= 0) 
					continue; 
				TypedArray arr = ctx.getTheme ().obtainStyledAttributes (resID, R.styleable.VerticalTextView); 
				try { 
					loadAttributeArray (arr, ctx, attrs); 
				} finally {
					arr.recycle (); 
				}
			} 
			else 
				Log.w ("VerticalTextView", "Unknown attribute"); 
		} 
	} 
	private void init (Context ctx, AttributeSet attrs) {
		TypedArray style = ctx.getTheme ().obtainStyledAttributes (attrs, R.styleable.VerticalTextView, 0, 0);
		try { 
			loadAttributeArray (style, ctx, attrs); 
		} finally { 
			style.recycle (); 
		} 
		widestCharacterWidth = (int) Math.ceil (mainPaint.measureText ("W", 0, 1)); 
	} 
	public VerticalTextView (Context ctx) { 
		super (ctx); 
	} 
	public VerticalTextView (Context ctx, AttributeSet attrs) { 
		super (ctx, attrs); 
		init (ctx, attrs); 
	} 
	public VerticalTextView (Context ctx, AttributeSet attrs, int defStyleAttr) { 
		super (ctx, attrs, defStyleAttr); 
		init (ctx, attrs); 
	} 
	
	@Override 
	protected void onMeasure (int wMeasureSpec, int hMeasureSpec) { 
		super.onMeasure (wMeasureSpec, hMeasureSpec); 
		int hMode = MeasureSpec.getMode (hMeasureSpec); 
		int hSize = MeasureSpec.getSize (hMeasureSpec); 
		int hMax = hSize; 
		if (hMode == MeasureSpec.UNSPECIFIED) hMax = 0; 
		measureOrLayout (hMax, false, measBounds); 
		setMeasuredDimension (measBounds.width (), measBounds.height ()); 
	}
	
	@Override public void onSizeChanged (int oldW, int oldH, int nowW, int nowH) { 
		super.onSizeChanged (oldW, oldH, nowW, nowH); 
	} 
	@Override protected void onLayout (boolean changed, int left, int top, int right, int bottom) { 
		super.onLayout (changed, left, top, right, bottom); 
		measureOrLayout (bottom - top, true, lMeasBounds); 
	} 
	
	@Override 
	protected void onDraw (Canvas canvas) { 
		super.onDraw (canvas); 
		String text = getDrawnText (); 
		mainPaint.setShadowLayer (shadowRadius, shadowDx, shadowDy, shadowColor); 
		boolean isLink; 
		int iHighlight; 
		int color = 0; 
		int szText = text.length (); 
		mainPaint.setTextSize ((int)(fontScale * textSize)); 
		for (int i = 0; i < szText; i++) { 
			isLink = whichLinkIsThisPosition (i) >= 0; 
			iHighlight = whichHighlightIsThisPosition (i); 
			if (!isLink) {
				color = (iHighlight >= 0) ? 
								(highlights.get (iHighlight).getBgColor ()) : 
								highlightColor; 
				if (color == 0) 
					color = highlightColor; 
			} 
			highlight.setColor (isLink? highlightColorLink: color); 
			canvas.drawRect (layoutBuffer.get (i).bounds, highlight); 
		} 
		for (int i = 0; i < szText; i++) { 
			isLink = whichLinkIsThisPosition (i) >= 0; 
			iHighlight = whichHighlightIsThisPosition (i); 
			if (!isLink) {
				color = (iHighlight >= 0) ? 
								(highlights.get (iHighlight).getTextColor ()) : 
								textColor; 
				if (color == 0) 
					color = textColor; 
			} 
			mainPaint.setColor (isLink? textColorLink: color); 
			CharacterPosition position = layoutBuffer.get (i); 
			switch (text.charAt (i)) {
				case '（': 
				case '）': 
					canvas.save (); 
					canvas.rotate (90, position.bounds.centerX (), position.bounds.centerY ()); 
					canvas.drawText (text, i, i + 1, position.x, position.y, mainPaint); 
					canvas.restore (); 
					break; 
				default: 
					canvas.drawText (text, i, i + 1, position.x, position.y, mainPaint); 
			} 
		} 
	} 
	
	private void measureOrLayout (int max_height, boolean needLayout, @Nullable Rect output_measured) { 
		int wCurrent = 0; 
		int wSoFar = 0; 
		int hSoFar = indentAllLines + indentFirstLine; 
		int hMax = max_height > 0? max_height - paddingBottom: 0; 
		int measHeight = 0; 
		int i; 
		String theText = getDrawnText (hMax); 
		int textLength = theText.length (); 
		mainPaint.setTextSize ((int)(fontScale * textSize)); 
		for (i = 0; i < textLength; i++) { 
			mainPaint.getTextBounds (theText, i, i + 1, bounds); 
			wCurrent = calcWCurrent (Math.max (bounds.right, bounds.width ())); 
			if (i > 0 && hMax > 0 && 
						calcBottomY (hSoFar) > hMax) { 
				// Reset vertical: 
				if (measHeight < hSoFar) measHeight = hSoFar; 
				wSoFar += wCurrent + spacingBetweenLines; 
				hSoFar = indentAllLines; 
			} 
			if (needLayout) 
				performCharacterLayout (i, wSoFar, hSoFar); 
			// Continue: 
			hSoFar = calcBottomY (hSoFar); // = bounds.top + bounds.height () 
		} 
		if (needLayout) 
			finishCharacterLayout (i); 
		wSoFar += wCurrent; 
		hSoFar = Math.max (measHeight, hSoFar); 
		if (output_measured != null) { 
			output_measured.set (0, 0, wSoFar + paddingLeft + paddingRight, 
										hSoFar + paddingTop + paddingBottom); 
		} 
	} 
	// Layout: 
	private void performCharacterLayout (int characterIndex, int wSoFar, int hSoFar) { 
		// Get X and Y: 
		int x = calcX (wSoFar); 
		int y = calcY (hSoFar); 
		// Calculate Character Background: 
		charBounds.set (calcLeftX (wSoFar), calcTopY (hSoFar), 
							   calcRightX (wSoFar), calcBottomY (hSoFar)); 
		updateCharacterPosition (characterIndex, charBounds, x, y); 
	} 
	private void finishCharacterLayout (int characterIndex) { 
		layoutBuffer.setSize (characterIndex); 
	} 
	// Calculate X: 
	private int calcX (int wSoFar) { 
		return calcRightX (wSoFar) - bounds.right - 
					   (int)(characterGridAlignLeft * (float)(calcWCurrent () - bounds.width ())); 
	} 
	private int calcLeftX (int wSoFar) { 
		return calcRightX (wSoFar) - calcWCurrent (); 
	} 
	private int calcRightX (int wSoFar) { 
		return getWidth () - wSoFar - paddingRight; 
	} 
	private int calcWCurrent () { 
		return widestCharacterWidth; 
	} 
	private int calcWCurrent (int someCharacterWidthToCheckAgainst) { 
		if (someCharacterWidthToCheckAgainst > widestCharacterWidth) 
			widestCharacterWidth = someCharacterWidthToCheckAgainst; 
		return widestCharacterWidth; 
	} 
	// Calculate Y: 
	private int calcY (int hSoFar) { 
		int hCurrent = calcHCurrent (); 
		return calcTopY (hSoFar) - bounds.top + 
					   (int)((1f - characterGridAlignTop) * (float)(hCurrent - bounds.height ())); 
	} 
	private int calcTopY (int hSoFar) { 
		return hSoFar + paddingTop; 
	} 
	private int calcBottomY (int hSoFar) { 
		return calcTopY (hSoFar) + calcHCurrent (); 
	} 
	private int calcHCurrent () { 
		return (int) Math.ceil (Math.max (bounds.height (), textSize)); 
	} 
	private int whichHighlightIsThisPosition (int position) { 
		int count = highlights.size (); 
		for (int i = 0; i < count; i++) { 
			Highlight highlight = highlights.get (i); 
			if (highlight.fromIndex <= position && 
					highlight.toIndex > position) 
				return i; 
		} 
		return -1; 
	} 
	private int whichLinkIsThisPosition (int position) { 
		int count = links.size (); 
		for (int i = 0; i < count; i++) { 
			Pair<Integer, Integer> link = links.get (i); 
			if (link.first <= position && 
					link.second > position) return i; 
		} 
		return -1; 
	} 
	private void updateCharacterPosition (int index, Rect bounds, int x, int y) { 
		CharacterPosition position; 
		if (index < layoutBuffer.size ()) { 
			position = layoutBuffer.get (index); 
		} else layoutBuffer.add (position = new CharacterPosition ()); 
		position.bounds.set (bounds); 
		position.set (x, y); 
	} 
	
	private static class CharacterPosition { 
		public Rect bounds = new Rect (); 
		public int x = 0; 
		public int y = 0; 
		// Methods: 
		public void set (int theX, int theY) { x = theX; y = theY; } 
	} 
	public static class Highlight { 
		public int fromIndex = 0; 
		public int toIndex = 0; 
		public Style style = null; 
		public static class Style { 
			public int bgColor = Color.TRANSPARENT; 
			public int textColor = 0; // 0 = same as surrounding text 
			public Style () {} 
			public Style (int highlightColor) { bgColor = highlightColor; } 
			public Style (int highlightColor, int foregroundColor) { 
				bgColor = highlightColor; 
				textColor = foregroundColor; 
			} 
		} 
		public Highlight () {} 
		public Highlight (int from, int to) { fromIndex = from; toIndex = to; } 
		public Highlight (int from, int to, Style s) { fromIndex = from; toIndex = to; style = s; } 
		public void setFromIndex (int index) { fromIndex = index; } 
		public void setToIndex (int index) { toIndex = index; } 
		public void setBgColor (int color) { if (style == null) style = new Style (); style.bgColor = color; } 
		public void setTextColor (int color) { if (style == null) style = new Style (); style.textColor = color; } 
		public void setStyle (Style style1) { style = style1; } 
		public int getFromIndex () { return fromIndex; } 
		public int getToIndex () { return toIndex; } 
		public int getBgColor () { return (style != null)? style.bgColor: Color.TRANSPARENT; } 
		public int getTextColor (){ return (style != null)? style.textColor: 0; } 
		public Style getStyle () { return style; } 
	} 
	
	public String getDrawnText (int max_height) {
		if ((max_height > 0 && (int)(fontScale * max_height) < 120) 
					&& !shortText.isEmpty ()) return shortText; 
		else return text; 
	} 
	public String getDrawnText () { 
		return getDrawnText (getHeight ()); 
	} 
	public Rect getDrawnBounds (int characterIndex) { 
		return layoutBuffer.get (characterIndex).bounds; 
	} 
	
	public void setText (String val) { text = val; highlights.clear (); links.clear (); 
		requestLayout (); invalidate (); } 
	public String getText () { return text; } 
	
	public void setShortText (String val) { shortText = val; highlights.clear (); links.clear (); 
		requestLayout (); invalidate (); } 
	public String getShortText () { return shortText; } 
	
	public void addHighlight (Highlight highlight) { highlights.add (highlight); } 
	public void addLink (int startPosition, int endPosition) { 
		links.add (new Pair<> (startPosition, endPosition)); 
	} 
	public boolean isPointInLink (int x, int y) { 
		return whichLinkIsPoint (x, y) >= 0; 
	} 
	public int whichLinkIsPoint (int x, int y) { return whichLinkIsPoint (x, y, 0); } 
	public int whichLinkIsPoint (int x, int y, int maxError) { 
		int count = links.size (); 
		for (int i = 0; i < count; i++) { 
			int from = links.get (i).first; 
			int to = links.get (i).second; 
			for (int j = from; j < to; j++) { 
				Rect r = getDrawnBounds (j); 
				if (x + maxError < r.left) continue; 
				if (x - maxError > r.right) continue; 
				if (y + maxError < r.top) continue; 
				if (y - maxError > r.bottom) continue; 
				return i; 
			} 
		} 
		return -1; 
	} 
	
	public void setFontScale (float scale) { fontScale = scale; widestCharacterWidth = 0; } 
	
	public void setHighlightColor (int c) { highlight.setColor (c); } 
} 
